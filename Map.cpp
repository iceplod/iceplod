/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Map.h"

#include "Action.h"
#include "Condition.h"
#include "WorldState.h"

#include <fstream>

enum class TileModType : quint32 {
    Portal,
    Enemy
};

struct PortalTileMod {
    quint32 x, y, id;

    void write(QDataStream &out) const
    {
        out << x << y << id;
    }

    void read(QDataStream &in)
    {
        in >> x >> y >> id;
    }
};

void EnemyTileMod::write(QDataStream &out) const
{
    out << x << y << quint32(type);
}

void EnemyTileMod::read(QDataStream &in)
{
    quint32 t;
    in >> x >> y >> t;
    type = (Enemy::Type)t;
}

struct MapFileHeader {
    qint32 magic;
    qint32 version;
    quint32 width;
    quint32 height;
    quint32 numTileMods;

    static const qint32 MagicNumber;
    static const qint32 CurrentVersion;

    void write(QDataStream &out) const
    {
        out << magic << version << width << height
            << numTileMods;
    }

    void read(QDataStream &in)
    {
        in >> magic >> version >> width >> height
            >> numTileMods;
    }
};

const qint32 MapFileHeader::MagicNumber = ('I' << 24) | ('P' << 16 ) |
        ('M' << 8) | 'D';
const qint32 MapFileHeader::CurrentVersion = 6;

Map::Map():
    tileGrid(NULL)
{
    setupDefault();
}

Map::Map(const QString &filename):
    tileGrid(NULL)
{
    setupDefault();
    setFilename(filename);
}

Map::~Map()
{
    cleanup();
}

void Map::getDrawInfo(GameState &gs)
{
    for(int y = 0; y < size.h; ++y){
        for(int x = 0; x < size.w; ++x){
            getTile(QPointF(x, y))->getDrawInfo(gs);
        }
    }
}

void Map::getEditorDrawInfo(GameState &gs, bool getPhys)
{
    getDrawInfo(gs);

    if(getPhys){
        for(int y = 0; y < size.h; ++y){
            for(int x = 0; x < size.w; ++x){
                getTile(QPointF(x, y))->getPhysDrawInfo(gs);
            }
        }
    }

    AnimationState as;
    as.size.set(1, 1);
    as.anim = StaticAnimations::Portal;
    for(QHash<QPointF, quint32>::const_iterator it = portals.constBegin();
            it != portals.constEnd(); ++it){
        as.pos = it.key();
        as.pos += QPointF(0.5f, 0.5f);
        gs.layer3Anims.push_back(as);

        TextItem text;
        text.pos = as.pos;
        text.size.set(0.2f, 0.4f);
        text.text = QString::number(it.value());
        gs.texts.push_back(text);
    }

    for(QList<EnemyTileMod>::const_iterator enemy = enemies.constBegin();
            enemy != enemies.constEnd(); ++enemy)
        Enemy::GetStaticDrawInfo(enemy->type,
                QPointF(enemy->x + 0.5f, enemy->y), gs);
}

void Map::paint(const QPointF &pos, Tile::Type type, AnimationHandle anim)
{
    ConstTilePtr p;

    if(type == Tile::Type::Nil){
        p = getTile(pos);
        type = p->type;
    }

    if(anim == StaticAnimations::Nil){
        if(!p)
            p = getTile(pos);
        anim = p->anim;
    }

    putTile(Tile::Create(pos, type, anim));
}

void Map::putTile(const ConstTilePtr &tile)
{
    if(tile->pos.x() < 0 || tile->pos.y() < 0 || tile->pos.x() >= size.w
            || tile->pos.y() >= size.h)
        return;
    tileGrid[(int)tile->pos.y() * size.w + (int)tile->pos.x()] = tile;
}

ConstTilePtr Map::getTile(const QPointF &pos) const
{
    if(pos.x() < 0 || pos.y() < 0 || pos.x() >= size.w || pos.y() >= size.h)
        return Tile::Create(pos, Tile::Type::Ignore);
    return tileGrid[(int)pos.y() * size.w + (int)pos.x()];
}

QPointF Map::findTileWithAnimation(AnimationHandle anim) const
{
    for(int x = 0; x < size.w; ++x){
        for(int y = 0; y < size.h; ++y){
            QPointF loc(x, y);
            if(getTile(loc)->anim == anim)
                return loc;
        }
    }

    return QPointF(-1, -1);
}

void Map::setFilename(const QString &filename)
{
    this->filename = filename;
    emit filenameChanged(this->filename);
}

QString Map::getFilename() const
{
    return filename;
}

QString Map::getName() const
{
    QFileInfo info(filename);
    return info.baseName();
}

void Map::cleanup()
{
    if(tileGrid){
        delete[] tileGrid;
        tileGrid = NULL;
    }

    size.w = 0;
    size.h = 0;
}

void Map::setupDefault()
{
    cleanup();

    size.set(100, 50);

    tileGrid = new ConstTilePtr[size.w * size.h];

    for(int y = 0; y < size.h; ++y){
        for(int x = 0; x < size.w; ++x){
            tileGrid[y * size.w + x] = Tile::Create(QPointF(x, y),
                    Tile::Type::Ignore);
        }
    }
}

void Map::save() const
{
    if(filename.isNull() || filename.isEmpty()){
        qCritical() << "Tried to save to blank filename.";
        return;
    }

    MapFileHeader header;
    header.magic = MapFileHeader::MagicNumber;
    header.version = MapFileHeader::CurrentVersion;
    header.width = size.w;
    header.height = size.h;
    header.numTileMods = portals.size() + enemies.size();

    QFile file(filename + ".ipm");
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    header.write(out);

    for(QHash<QPointF, quint32>::const_iterator portal = portals.constBegin();
            portal != portals.constEnd(); ++portal){
        PortalTileMod mod;
        mod.x = quint32(portal.key().x());
        mod.y = quint32(portal.key().y());
        mod.id = portal.value();
        out << quint32(TileModType::Portal);
        mod.write(out);
    }

    for(QList<EnemyTileMod>::const_iterator enemy = enemies.constBegin();
            enemy != enemies.constEnd(); ++enemy){
        out << quint32(TileModType::Enemy);
        enemy->write(out);
    }

    for(int y = 0; y < size.h; ++y){
        for(int x = 0; x < size.w; ++x){
            getTile(QPointF(x, y))->write(out);
        }
    }
}

void Map::load()
{
    QFile file(filename + ".ipm");

    if(!file.exists())
        return;

    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    MapFileHeader header;
    header.read(in);

    if(header.magic != MapFileHeader::MagicNumber){
        emit error("This isn't an IcePlod map file: " + filename + ".ipm");
        setupDefault();
        return;
    }

    if(header.version != MapFileHeader::CurrentVersion){
        emit error("Invalid IcePlod map version: " + header.version);
        setupDefault();
        return;
    }

    size.set(header.width, header.height);

    for(int i = 0; i < header.numTileMods; ++i){
        quint32 type;

        in >> type;

        switch((TileModType)type){
        case TileModType::Portal:
            {
                PortalTileMod mod;
                mod.read(in);
                portals.insert(QPointF(mod.x, mod.y), mod.id);
            }
            break;
        case TileModType::Enemy:
            {
                EnemyTileMod mod;
                mod.read(in);
                enemies.push_back(mod);
            }
            break;
        default:
            emit error("Unknown tile mod type: " + type);
            break;
        }
    }

    tileGrid = new ConstTilePtr[size.w * size.h];

    for(int y = 0; y < size.h; ++y){
        for(int x = 0; x < size.w; ++x){
            putTile(Tile::Read(QPointF(x, y), in));
        }
    }

    loadScript();
}

void Map::loadScript()
{
    try{
        std::ifstream in(filename.toStdString() + ".scr");

        YAML::Parser parser(in);
        YAML::Node doc;

        /* script file doesn't exist */
        if(!parser.GetNextDocument(doc))
            return;

        const YAML::Node *triggersNode = doc.FindValue("triggers");
        if(triggersNode)
            for(YAML::Iterator triggerIt = triggersNode->begin();
                    triggerIt != triggersNode->end(); ++triggerIt){
                Trigger trigger = Trigger::CreateFromNode(*triggerIt);
                triggers.insert(trigger.getName(), trigger);
            }

        const YAML::Node *dialogsNode = doc.FindValue("dialogs");
        if(dialogsNode)
            for(YAML::Iterator dialogIt = dialogsNode->begin();
                    dialogIt != dialogsNode->end(); ++dialogIt){
                DialogTreePtr p(new DialogTree(*dialogIt));
                dialogs.insert(p->getName(), p);
            }
    }catch(YAML::Exception e){
        emit error(QString("YAML exception: ") + e.what());
        return;
    }
}

void Map::resize(Rect<int> newSize)
{
    if(newSize.w <= 0 || newSize.h <= 0)
        return;

    ConstTilePtr *newTileGrid = new ConstTilePtr[newSize.w * newSize.h];
    for(int y = 0; y < newSize.h; ++y){
        for(int x = 0; x < newSize.w; ++x){
            newTileGrid[y * newSize.w + x] = getTile(QPointF(x, y));
        }
    }
    delete[] tileGrid;
    tileGrid = newTileGrid;
    size.set(newSize.w, newSize.h);
}

DialogTreePtr Map::getDialogTree(const QString &name)
{
    if(!dialogs.contains(name))
        return DialogTreePtr();
    return dialogs[name];
}

void Map::togglePortal(const QPointF &pos, bool on)
{
    if(!on){
        if(portals.contains(pos)){
            quint32 portal = portals.take(pos);
            emit portalDeleted(getName(), portal);
        }
        return;
    }

    if(portals.contains(pos))
        return;

    QList<quint32> portalNums = portals.values();
    std::sort(portalNums.begin(), portalNums.end());
    quint32 num = 1;
    for(QList<quint32>::const_iterator it = portalNums.constBegin();
            it != portalNums.constEnd(); ++it){
        if(*it > num)
            break;
        ++num;
    }
    portals.insert(pos, num);
    emit portalAdded(getName(), num);
}

QList<quint32> Map::getPortals() const
{
    QList<quint32> ret = portals.values();
    std::sort(ret.begin(), ret.end());
    return ret;
}

QPointF Map::getPortalLoc(quint32 portal) const
{
    return portals.key(portal);
}

void Map::placeEnemy(QPointF pos, Enemy::Type type)
{
    EnemyTileMod enemy;
    enemy.x = pos.x();
    enemy.y = pos.y();
    enemy.type = type;
    enemies.push_back(enemy);
}

void Map::removeEnemy(QPointF pos)
{
    for(QList<EnemyTileMod>::iterator enemy = enemies.begin();
            enemy != enemies.end(); ++enemy){
        if(enemy->x == pos.x() && enemy->y == pos.y()){
            enemy = enemies.erase(enemy);
            if(enemy == enemies.end())
                break;
        }
    }
}

void Map::createInitialEnemies(QList<EnemyPtr> &out) const
{
    for(QList<EnemyTileMod>::const_iterator enemy = enemies.constBegin();
            enemy != enemies.constEnd(); ++enemy){
        EnemyPtr p = Enemy::Create(enemy->type,
                QPointF(enemy->x + 0.5f, enemy->y));
        out.push_back(p);
    }
}

void Map::createPortalTrigger(quint32 portal, const ExitData &exit)
{
    Trigger trigger;

    ConditionPtr c(new PlayerActionCondition);
    trigger.addCondition(c);

    QPointF loc = getPortalLoc(portal);
    c.reset(new PlayerPositionCondition(loc, loc));
    trigger.addCondition(c);

    ActionPtr a(new GoToPortalAction(exit.portal, exit.map, exit.levelset));
    trigger.addAction(a);

    triggers.insert("__portal" + QString::number(portal), trigger);
}

bool Map::evaluateTriggers(WorldState &worldState)
{
    bool ret = false;
    for(QHash<QString, Trigger>::iterator trigger = triggers.begin();
            trigger != triggers.end(); ++trigger)
        ret = trigger.value().evaluate(worldState) || ret;
    return ret;
}

void Map::forceTrigger(const QString &name, WorldState &worldState)
{
    if(!triggers.contains(name)){
        emit error("Tried to force unknown trigger: " + name);
        return;
    }

    Trigger &trigger = triggers[name];
    trigger.force(worldState);
}
