TEMPLATE = app

iped {
    TARGET = iped
} else {
    TARGET = iceplod
}

CONFIG += qt

QT += opengl

QMAKE_CXXFLAGS += \
    -std=c++0x \
    -Wall \
    -Wno-reorder \
    -Wno-sign-compare \

LIBS += \
    -Lyaml-cpp/Build/yaml-cpp/lib -lyaml-cpp \

INCLUDEPATH += \
    yaml-cpp/Build/yaml-cpp/include \

DEFINES += \
    IP_DEBUG \

HEADERS += \
    Action.h \
    Actor.h \
    Animation.h \
    Condition.h \
    Constants.h \
    DialogTree.h \
    Enemy.h \
    ExitData.h \
    GameState.h \
    LevelSet.h \
    LevelSet_p.h \
    Map.h \
    Math.h \
    Player.h \
    Renderer.h \
    Simulation.h \
    Tile.h \
    Trigger.h \
    Weapon.h \
    WorldState.h \

iped {
    HEADERS += \
        ToolDockContents.h \
        ToolDockContents_p.h \
        Editor.h \
        Editor_p.h \

} else {
    HEADERS += \
        IcePlod.h \

}

SOURCES += \
    Action.cpp \
    Animation.cpp \
    Condition.cpp \
    DialogTree.cpp \
    Enemy.cpp \
    GameState.cpp \
    KeyState.cpp \
    LevelSet.cpp \
    Map.cpp \
    Math.cpp \
    Player.cpp \
    Renderer.cpp \
    Simulation.cpp \
    Tile.cpp \
    Trigger.cpp \
    Weapon.cpp \

iped {
    SOURCES += \
        Editor.cpp \
        ToolDockContents.cpp \

} else {
    SOURCES += \
        IcePlod.cpp \

}
