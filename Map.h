/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAP_H
#define MAP_H

#include "Constants.h"
#include "DialogTree.h"
#include "Enemy.h"
#include "Trigger.h"
#include "ExitData.h"
#include "Math.h"
#include "Tile.h"

#include <QString>

struct WorldState;

struct EnemyTileMod {
    quint32 x, y;
    Enemy::Type type;
    void write(QDataStream &out) const;
    void read(QDataStream &in);
};

class Map : public QObject {
    Q_OBJECT

    public:
        Map();
        Map(const QString &filename);
        virtual ~Map();

        void setupDefault();

        void setFilename(const QString &filename);
        QString getFilename() const;
        QString getName() const;

        void save() const;
        void load();
        void loadScript();

        void getDrawInfo(GameState &gs);
        void getEditorDrawInfo(GameState &gs, bool includePhys);

        void paint(const QPointF &pos, Tile::Type type, AnimationHandle anim);
        void putTile(const ConstTilePtr &tile);
        ConstTilePtr getTile(const QPointF &pos) const;
        const ConstTilePtr *getTileData() const { return tileGrid; }
        QPointF findTileWithAnimation(AnimationHandle anim) const;

        void resize(Rect<int> size);

        bool evaluateTriggers(WorldState &worldState);
        void forceTrigger(const QString &trigger, WorldState &worldState);
        DialogTreePtr getDialogTree(const QString &name);

        void togglePortal(const QPointF &pos, bool on);
        QList<quint32> getPortals() const;
        QPointF getPortalLoc(quint32 portal) const;
        void createPortalTrigger(quint32 portal, const ExitData &exit);

        void placeEnemy(QPointF pos, Enemy::Type type);
        void removeEnemy(QPointF pos);
        void createInitialEnemies(QList<EnemyPtr> &enemies) const;

        const Rect<int> &getSize() const
        {
            return size;
        }

    signals:
        void filenameChanged(QString newFilename);
        void error(QString str);

        void portalAdded(QString map, quint32 id);
        void portalDeleted(QString map, quint32 id);

    protected:
        ConstTilePtr *tileGrid;
        QString filename;
        Rect<int> size;
        QHash<QPointF, quint32> portals;
        QList<EnemyTileMod> enemies;
        QHash<QString, Trigger> triggers;
        QHash<QString, DialogTreePtr> dialogs;

        void cleanup();
};

typedef boost::shared_ptr<Map> MapPtr;

#endif
