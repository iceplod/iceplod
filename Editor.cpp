/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Editor.h"
#include "Editor_p.h"

#include "LevelSet.h"
#include "ToolDockContents.h"

#include <QPoint>
#include <QProcess>

Editor::Editor(QString filename, QWidget *parent):
    QMainWindow(parent),
    tool(NULL)
{
    setWindowTitle("IcePlod Level Editor");
    setWindowState(Qt::WindowMaximized);

    connect(&renderer, SIGNAL(error(QString)),
            this, SLOT(displayError(QString)));
    connect(&renderer, SIGNAL(mouseMove(QMouseEvent *)),
            this, SLOT(rendererMouseMoveEvent(QMouseEvent *)));

    setCentralWidget(&renderer);

    setupStatusBar();

    setupDocks();

    setupMenus();

    QSettings settings(Organization, Application);
    restoreGeometry(settings.value("editor/geometry").toByteArray());
    restoreState(settings.value("editor/windowstate").toByteArray());

    /* initialize the renderer */
    renderer.updateGL();

    if(!filename.isEmpty())
        loadLevelSet(filename);

    resendGameState();

    QTimer *rtimer = new QTimer(this);
    connect(rtimer, SIGNAL(timeout()), &renderer, SLOT(updateGL()));
    rtimer->start(16.6);
}

void Editor::setupDocks()
{
    brushDock = new QDockWidget("Brush", this);
    brushDock->setObjectName("brushdock");
    brushDock->setAllowedAreas(Qt::LeftDockWidgetArea |
            Qt::RightDockWidgetArea);
    brushDock->setFeatures(QDockWidget::DockWidgetClosable |
            QDockWidget::DockWidgetMovable |
            QDockWidget::DockWidgetFloatable);
    brushDock->setFloating(true);
    toolDockContents = new ToolDockContents;
    brushDock->setWidget(toolDockContents);
    addDockWidget(Qt::LeftDockWidgetArea, brushDock);
    connect(toolDockContents, SIGNAL(newTool(Tool *)), this,
            SLOT(newTool(Tool *)));
    connect(toolDockContents, SIGNAL(toolNameChanged(QString)),
            brushDock, SLOT(setWindowTitle(const QString &)));
    toolDockContents->emitTool();

    mapDock = new QDockWidget("Maps", this);
    mapDock->setObjectName("mapdock");
    mapDock->setAllowedAreas(Qt::LeftDockWidgetArea |
            Qt::RightDockWidgetArea);
    mapDock->setFeatures(QDockWidget::DockWidgetClosable |
            QDockWidget::DockWidgetMovable |
            QDockWidget::DockWidgetFloatable);
    mapDock->setFloating(false);
    mapDockContents = new MapDockContents;
    mapDock->setWidget(mapDockContents);
    connect(mapDockContents, SIGNAL(mapSelected(MapPtr)),
            this, SLOT(mapSelected(MapPtr)));
    addDockWidget(Qt::LeftDockWidgetArea, mapDock);
}

void Editor::setupMenus()
{
    QMenu *fileMenu = menuBar()->addMenu("File");

    QAction *newAct = fileMenu->addAction("New LevelSet...");
    newAct->setShortcut(QKeySequence("Ctrl+N"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newLevelSet()));

    QAction *loadAct = fileMenu->addAction("Load LevelSet...");
    loadAct->setShortcut(QKeySequence("Ctrl+O"));
    connect(loadAct, SIGNAL(triggered()), this, SLOT(load()));

    fileMenu->addSeparator();

    QAction *saveAct = fileMenu->addAction("Save LevelSet");
    saveAct->setShortcut(QKeySequence("Ctrl+S"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    fileMenu->addSeparator();

    QAction *runAct = fileMenu->addAction("Run LevelSet");
    runAct->setShortcut(QKeySequence("Ctrl+R"));
    connect(runAct, SIGNAL(triggered()), this, SLOT(testLevel()));

    fileMenu->addSeparator();

    QAction *quitAct = fileMenu->addAction("Quit");
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    QMenu *lsMenu = menuBar()->addMenu("LevelSet");

    QAction *lsPropertiesAct = lsMenu->addAction("LevelSet Properties...");
    lsPropertiesAct->setShortcut(QKeySequence("Alt+Enter"));
    connect(lsPropertiesAct, SIGNAL(triggered()),
            this, SLOT(editLevelSetProperties()));

    QAction *lsResEdAct = lsMenu->addAction("Edit resources...");
    lsResEdAct->setShortcut(QKeySequence("Ctrl+E"));
    connect(lsResEdAct, SIGNAL(triggered()),
            this, SLOT(editResources()));

    QMenu *mapMenu = menuBar()->addMenu("Map");

    QAction *resizeAct = mapMenu->addAction("Resize...");
    connect(resizeAct, SIGNAL(triggered()), this, SLOT(resize()));

    QAction *editPortalsAct = mapMenu->addAction("Edit Portal Bindings...");
    editPortalsAct->setShortcut(QKeySequence("Ctrl+P"));
    connect(editPortalsAct, SIGNAL(triggered()), this, SLOT(editPortals()));

    QMenu *viewMenu = menuBar()->addMenu("View");

    toggleGridAct = viewMenu->addAction("Grid");
    toggleGridAct->setCheckable(true);
    toggleGridAct->setChecked(true);
    toggleGridAct->setShortcut(QKeySequence("Ctrl+G"));
    connect(toggleGridAct, SIGNAL(triggered()), this, SLOT(resendGameState()));

    togglePhysAct = viewMenu->addAction("Tile physics");
    togglePhysAct->setCheckable(true);
    togglePhysAct->setChecked(true);
    togglePhysAct->setShortcut(QKeySequence("Ctrl+B"));
    connect(togglePhysAct, SIGNAL(triggered()), this, SLOT(resendGameState()));

    QMenu *windowMenu = menuBar()->addMenu("Window");

    QAction *toggleBrushDockAct = brushDock->toggleViewAction();
    toggleBrushDockAct->setCheckable(true);
    toggleBrushDockAct->setChecked(true);
    windowMenu->addAction(toggleBrushDockAct);

    QAction *toggleMapDockAct = mapDock->toggleViewAction();
    toggleMapDockAct->setCheckable(true);
    toggleMapDockAct->setChecked(true);
    windowMenu->addAction(toggleMapDockAct);
}

void Editor::setupStatusBar()
{
    QStatusBar *bar = statusBar();

    bar->setSizeGripEnabled(false);

    coordsLabel = new QLabel;
    coordsLabel->setText("(, )");
    bar->addPermanentWidget(coordsLabel);

    zoomLabel = new QLabel;
    zoomLabel->setText("100%");
    bar->addPermanentWidget(zoomLabel);

    zoomSlider = new QSlider(Qt::Horizontal);
    zoomSlider->setMaximum(100);
    zoomSlider->setMinimum(10);
    zoomSlider->setSingleStep(10);
    zoomSlider->setPageStep(30);
    zoomSlider->setValue(100);
    connect(zoomSlider, SIGNAL(valueChanged(int)),
            this, SLOT(zoomChanged(int)));
    bar->addPermanentWidget(zoomSlider);
}

void Editor::closeEvent(QCloseEvent *event)
{
    if(levelset){
        QMessageBox::StandardButton res = QMessageBox::question(this,
                "Save before exit?",
                "Do you want to save before exiting?",
                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                QMessageBox::Cancel);

        if(res == QMessageBox::Cancel){
            event->ignore();
            return;
        }

        if(res == QMessageBox::Yes)
            levelset->save();
    }

    QSettings settings(Organization, Application);
    settings.setValue("editor/geometry", saveGeometry());
    settings.setValue("editor/windowstate", saveState());
    event->accept();
}

void Editor::mapSelected(MapPtr map)
{
    this->map = map;

    cameraFocus.rx() = 0;
    cameraFocus.ry() = 0;

    resendGameState();
}

void Editor::newLevelSet()
{
    QString filename = QFileDialog::getSaveFileName(this, "New LevelSet",
            QString(), QString(), NULL, QFileDialog::ShowDirsOnly |
            QFileDialog::HideNameFilterDetails);

    if(filename.isEmpty())
        return;

    loadLevelSet(filename, true);

    mapDockContents->newMapClicked();
}

void Editor::loadLevelSet(QString filename, bool create)
{
    map.reset();
    toolDockContents->reset();
    mapDockContents->reset();
    renderer.resetUserAnims();
    resendGameState();
    levelset.reset(new LevelSet(filename));
    connect(levelset.get(), SIGNAL(error(QString)),
            this, SLOT(displayError(QString)));
    connect(levelset.get(), SIGNAL(animationAdded(
                    AnimationHandle, QString, QString)),
            &renderer, SLOT(addUserAnim(
                    AnimationHandle, QString, QString)));
    connect(levelset.get(), SIGNAL(animationDeleted(AnimationHandle)),
            &renderer, SLOT(deleteUserAnim(AnimationHandle)));
    toolDockContents->hookupLevelSet(levelset);
    mapDockContents->hookupLevelSet(levelset);
    if(!levelset->load(create))
        levelset.reset();
}

void Editor::load()
{
    QString filename = QFileDialog::getExistingDirectory(this,
            "Open IcePlod LevelSet");

    if(filename.isNull() || filename.isEmpty())
        return;

    loadLevelSet(filename);

    resendGameState();
    statusBar()->showMessage(filename + " loaded.", 3);
}

void Editor::save()
{
    if(!levelset)
        return;

    levelset->save();
}

void Editor::testLevel()
{
    if(!levelset)
        return;

    QString program = "./iceplod";

    QStringList args(levelset->getFilename());

    QProcess *game = new QProcess(this);
    game->startDetached(program, args);
}

ResizeDialog::ResizeDialog(Rect<int> size, QWidget *parent):
    QDialog(parent)
{
    setWindowTitle("Resize Map");

    QVBoxLayout *lt = new QVBoxLayout;

    QLabel *lbl = new QLabel("Set map size:");
    lt->addWidget(lbl);

    QHBoxLayout *hlt = new QHBoxLayout;

    lbl = new QLabel("Width:");
    hlt->addWidget(lbl);

    widthEdit = new QSpinBox;
    widthEdit->setRange(0, INT_MAX);
    widthEdit->setValue(size.w);
    hlt->addWidget(widthEdit);

    lbl = new QLabel("Height:");
    hlt->addWidget(lbl);

    heightEdit = new QSpinBox;
    heightEdit->setRange(0, INT_MAX);
    heightEdit->setValue(size.h);
    hlt->addWidget(heightEdit);

    lt->addLayout(hlt);

    QDialogButtonBox *box = new QDialogButtonBox(QDialogButtonBox::Ok |
            QDialogButtonBox::Cancel);
    connect(box, SIGNAL(accepted()), this, SLOT(accept()));
    connect(box, SIGNAL(rejected()), this, SLOT(reject()));
    lt->addWidget(box);

    setLayout(lt);
}

Rect<int> ResizeDialog::getSize()
{
    return Rect<int>(widthEdit->value(), heightEdit->value());
}

void Editor::resize()
{
    if(!map)
        return;
    ResizeDialog dlg(map->getSize(), this);
    int res = dlg.exec();
    if(res == QDialog::Accepted){
        Rect<int> size = dlg.getSize();
        qDebug() << "got size:" << size;
        map->resize(size);
        resendGameState();
    }
}

EditPortalsDialog::EditPortalsDialog(QAbstractTableModel *model,
        LevelSetPtr ls, QWidget *parent):
    QDialog(parent),
    ls(ls),
    model(model)
{
    setWindowTitle("Edit Portal Bindings");

    QFormLayout *layout = new QFormLayout;

    portalList = new QListView;
    portalList->setModelColumn(LevelSet::PortalModelColumns::Summary);
    portalList->setModel(model);
    connect(portalList, SIGNAL(activated(const QModelIndex &)), this,
            SLOT(portalSelected(const QModelIndex &)));
    layout->addRow("Source Portal:", portalList);

    dstPortal = new QLineEdit;
    layout->addRow("Destination Portal:", dstPortal);

    dstMap = new QComboBox;
    dstMap->setMinimumContentsLength(40);
    dstMap->setEditable(true);
    QList<QString> mapNames = ls->getMapNames();
    for(QList<QString>::const_iterator name = mapNames.constBegin();
            name != mapNames.constEnd(); ++name){
        dstMap->addItem(*name);
    }
    dstMap->lineEdit()->setText(QString());
    layout->addRow("Destination Map:", dstMap);

    dstLevelSet = new QLineEdit;
    layout->addRow("Destination LevelSet:", dstLevelSet);

    QDialogButtonBox *box = new QDialogButtonBox(QDialogButtonBox::Ok |
            QDialogButtonBox::Cancel);
    connect(box, SIGNAL(accepted()), this, SLOT(accept()));
    connect(box, SIGNAL(rejected()), this, SLOT(reject()));
    layout->addWidget(box);

    setLayout(layout);
}

void EditPortalsDialog::portalSelected(const QModelIndex &index)
{
    QModelIndex idx;

    if(curRow != -1){
        idx = model->index(curRow, LevelSet::PortalModelColumns::DstPortal);
        model->setData(idx, QVariant(dstPortal->text()));

        idx = model->index(curRow, LevelSet::PortalModelColumns::DstLevelSet);
        model->setData(idx, QVariant(dstLevelSet->text()));

        idx = model->index(curRow, LevelSet::PortalModelColumns::DstMap);
        model->setData(idx, QVariant(dstMap->lineEdit()->text()));
    }

    curRow = index.row();

    idx = model->index(curRow, LevelSet::PortalModelColumns::DstPortal);
    dstPortal->setText(model->data(idx).toString());

    idx = model->index(curRow, LevelSet::PortalModelColumns::DstLevelSet);
    dstLevelSet->setText(model->data(idx).toString());

    idx = model->index(curRow, LevelSet::PortalModelColumns::DstMap);
    dstMap->lineEdit()->setText(model->data(idx).toString());
}

void Editor::editPortals()
{
    if(!map)
        return;

    QAbstractTableModel *model = levelset->newPortalModel(map->getName());

    EditPortalsDialog dlg(model, levelset, this);
    int res = dlg.exec();
    if(res == QDialog::Accepted)
        levelset->commitPortalModel(map->getName(), model);

    delete model;
}

LevelSetPropertiesDialog::LevelSetPropertiesDialog(LevelSetPtr ls,
        QWidget *parent):
    QDialog(parent),
    ls(ls)
{
    setWindowTitle("LevelSet Properties");

    QFormLayout *layout = new QFormLayout;

    startMap = new QComboBox;
    layout->addRow("Start map:", startMap);

    startPortal = new QComboBox;
    startPortal->setEnabled(false);
    layout->addRow("Start portal:", startPortal);

    startMap->addItem("(No Start)");
    QList<QString> maps = ls->getMapNames();
    for(QList<QString>::const_iterator map = maps.constBegin();
            map != maps.constEnd(); ++map)
        startMap->addItem(*map);

    connect(startMap, SIGNAL(currentIndexChanged(const QString &)),
            this, SLOT(mapIndexChanged(const QString &)));

    MapPtr map = ls->getStartMap();
    if(map){
        QString curMap = map->getName();
        if(!curMap.isEmpty()){
            int idx = startMap->findText(curMap);
            if(idx >= 0)
                startMap->setCurrentIndex(idx);

            quint32 portal = ls->getStartPortal();
            idx = startPortal->findText(QString::number(portal));
            if(idx >= 0)
                startPortal->setCurrentIndex(idx);
        }
    }

    QDialogButtonBox *box = new QDialogButtonBox(QDialogButtonBox::Ok |
            QDialogButtonBox::Cancel);
    connect(box, SIGNAL(accepted()), this, SLOT(accept()));
    connect(box, SIGNAL(rejected()), this, SLOT(reject()));
    layout->addWidget(box);

    setLayout(layout);
}

void LevelSetPropertiesDialog::mapIndexChanged(const QString &mapName)
{
    startPortal->clear();

    if(mapName.isEmpty() || startMap->currentIndex() <= 0){
        startPortal->setEnabled(false);
        return;
    }

    MapPtr map = ls->getMap(mapName);

    QList<quint32> portals = map->getPortals();
    for(QList<quint32>::const_iterator portal = portals.constBegin();
            portal != portals.constEnd(); ++portal)
        startPortal->addItem(QString::number(*portal));

    startPortal->setEnabled(true);
}

QString LevelSetPropertiesDialog::getStartMap() const
{
    if(startMap->currentIndex() <= 0)
        return QString();

    return startMap->currentText();
}

quint32 LevelSetPropertiesDialog::getStartPortal() const
{
    return startPortal->currentText().toUInt();
}

void Editor::editLevelSetProperties()
{
    if(!levelset)
        return;

    LevelSetPropertiesDialog dlg(levelset, this);
    int res = dlg.exec();
    if(res == QDialog::Accepted){
        levelset->setStartMap(dlg.getStartMap());
        levelset->setStartPortal(dlg.getStartPortal());
    }
}

void Editor::editResources()
{
    if(!levelset)
        return;

    ResourceEditorDialog dlg(levelset, this);
    connect(&dlg, SIGNAL(error(QString)), this, SLOT(displayError(QString)));
    dlg.exec();
}

void Editor::mapFilenameChanged(QString newFilename)
{
    setWindowTitle("IcePlod Map Editor - " + newFilename);
}

void Editor::zoomChanged(int zoom)
{
    zoomLabel->setText(QString::number(zoom) + "%");
    resendGameState();
}

void Editor::resendGameState()
{
    GameState gs;

    if(!map){
        GameState::SetGameState(gs);
        return;
    }

    gs.zoom = zoomSlider->value() / 100.f;
    gs.cameraFocus = cameraFocus;

    Rect<int> size = map->getSize();
    gs.lines.push_back(Line(QPointF(0.f, 0.f), QPointF(size.w, 0.f)));
    gs.lines.push_back(Line(QPointF(size.w, 0.f),
                QPointF(size.w, size.h)));
    gs.lines.push_back(Line(QPointF(size.w, size.h),
                QPointF(0.f, size.h)));
    gs.lines.push_back(Line(QPointF(0.f, size.h), QPointF(0.f, 0.f)));

    map->getEditorDrawInfo(gs, togglePhysAct->isChecked());

    if(toggleGridAct->isChecked()){
        int lowX = cameraFocus.x() - Renderer::GameViewport.hw;
        lowX = lowX > 0 ? lowX : 0;

        int lowY = cameraFocus.y() - Renderer::GameViewport.hh;
        lowY = lowY > 0 ? lowY : 0;

        int highX = cameraFocus.x() + Renderer::GameViewport.hw;
        highX = highX < map->getSize().w ? highX : map->getSize().w;

        int highY = cameraFocus.y() + Renderer::GameViewport.hh;
        highY = (highY < map->getSize().h ? highY : map->getSize().h) + 1;

        for(int x = lowX; x <= highX; ++x)
            gs.lines.push_back(Line(QPointF(x, lowY), QPointF(x, highY),
                        Line::Style_Thin | Line::Style_Dashed));

        for(int y = lowY; y <= highY; ++y)
            gs.lines.push_back(Line(QPointF(lowX, y), QPointF(highX, y),
                        Line::Style_Thin | Line::Style_Dashed));
    }

    if(tool)
        tool->getDrawInfo(gs, lastMouseMapCoord);

    GameState::SetGameState(gs);
}

void Editor::newTool(Tool *newTool)
{
    tool = newTool;
}

void Editor::mousePressEvent(QMouseEvent *event)
{
    event->accept();

    lastMousePos = renderer.mapFromParentToGame(event->pos());
}

void Editor::mouseReleaseEvent(QMouseEvent *event)
{
    event->accept();

    if(!map)
        return;

    QPointF gamePoint(renderer.mapFromParentToGame(event->pos()));
    Rect<float> tos = renderer.tilesOnScreen();
    QPointF mapCoord(
            (int)(gamePoint.x() - tos.hw + cameraFocus.x()),
            (int)(gamePoint.y() - tos.hh + cameraFocus.y()));

    if(lastMousePos == gamePoint){
        if(tool)
            tool->work(*(map.get()), mapCoord, event->button(), false);

        resendGameState();
    }
}

void Editor::rendererMouseMoveEvent(QMouseEvent *event)
{
    processMoveEvent(event, false);
}

void Editor::mouseMoveEvent(QMouseEvent *event)
{
    processMoveEvent(event, true);
}

void Editor::processMoveEvent(QMouseEvent *event, bool shove)
{
    event->accept();

    if(!map)
        return;

    QPointF gamePoint;
    if(shove)
        gamePoint = renderer.mapFromParentToGame(event->pos());
    else
        gamePoint = renderer.mapToGame(event->pos());
    QPointF rel(gamePoint - lastMousePos);
    lastMousePos = gamePoint;
    Rect<float> tos = renderer.tilesOnScreen();
    QPointF mapCoord((int)(gamePoint.x() - tos.hw + cameraFocus.x()),
            (int)(gamePoint.y() - tos.hh + cameraFocus.y()));
    lastMouseMapCoord = mapCoord;

    coordsLabel->setText("(" + QString::number(mapCoord.x()) + ", " +
            QString::number(mapCoord.y()) + ")");

    if(event->buttons() & Qt::MidButton){
        cameraFocus -= 2 * rel;

        resendGameState();
        return;
    }

    if(tool)
        tool->work(*(map.get()), mapCoord, event->buttons(), true);

    resendGameState();
}

void Editor::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0)
        zoomSlider->triggerAction(QAbstractSlider::SliderSingleStepAdd);
    else if(event->delta() < 0)
        zoomSlider->triggerAction(QAbstractSlider::SliderSingleStepSub);;
}

void Editor::displayError(QString error)
{
    qDebug() << error;
    QMessageBox msg;
    msg.setWindowTitle("Error");
    msg.setIcon(QMessageBox::Warning);
    msg.setText(error);
    msg.exec();
}

MapDockContents::MapDockContents(QWidget *parent):
    QWidget(parent)
{
    QGridLayout *layout = new QGridLayout;

    mapList = new QListWidget;
    layout->addWidget(mapList, 0, 0, 1, 2);

    newMapButton = new QPushButton(QIcon("icons/new_map.png"), QString());
    layout->addWidget(newMapButton, 1, 0, 1, 1);

    deleteMapButton = new QPushButton(QIcon("icons/delete_map.png"),
            QString());
    layout->addWidget(deleteMapButton, 1, 1, 1, 1);

    setLayout(layout);
}

void MapDockContents::reset()
{
    mapList->clear();
    ls.reset();
}

void MapDockContents::newMapClicked()
{
    if(!ls)
        return;

    bool ok;
    QString name = QInputDialog::getText(this, "New Map Name", "Map Name:",
            QLineEdit::Normal, QString(), &ok);
    if(!ok || name.isEmpty())
        return;

    ls->addNewMap(name);
}

void MapDockContents::deleteMapClicked()
{
    if(!ls)
        return;

    QListWidgetItem *item = mapList->currentItem();
    if(!item)
        return;

    QString name = item->text();
    if(name.isEmpty())
        return;

    QMessageBox::StandardButton response = QMessageBox::question(this,
            "Confirm Map Deletion",
            QString("Are you sure you want to delete ") + name + "?",
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if(response != QMessageBox::Yes)
        return;

    ls->deleteMap(name);
}

void MapDockContents::lsMapAdded(QString name)
{
    mapList->addItem(name);
}

void MapDockContents::lsMapDeleted(QString name)
{
    QList<QListWidgetItem*> foundMaps = mapList->findItems(name,
            Qt::MatchExactly);

    if(foundMaps.count() <= 0)
        return;

    QListWidgetItem *item = foundMaps.first();
    int row = mapList->row(item);
    mapList->takeItem(row);
    delete item;
}

void MapDockContents::mapListDoubleClicked(QListWidgetItem *item)
{
    if(!ls)
        return;

    MapPtr map = ls->getMap(item->text());
    if(!map)
        return;

    emit mapSelected(map);
}

void MapDockContents::hookupLevelSet(LevelSetPtr ls)
{
    this->ls = ls;
    connect(ls.get(), SIGNAL(mapAdded(QString)),
            this, SLOT(lsMapAdded(QString)));
    connect(ls.get(), SIGNAL(mapDeleted(QString)),
            this, SLOT(lsMapDeleted(QString)));
    connect(newMapButton, SIGNAL(clicked()),
            this, SLOT(newMapClicked()));
    connect(deleteMapButton, SIGNAL(clicked()),
            this, SLOT(deleteMapClicked()));
    connect(mapList, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
            this, SLOT(mapListDoubleClicked(QListWidgetItem*)));
}

ResourceEditorDialog::ResourceEditorDialog(LevelSetPtr ls, QWidget *parent):
    QDialog(parent),
    ls(ls)
{
    tabWidget = new QTabWidget;

    setupAnimationsTab();

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(tabWidget);
    layout->addWidget(buttonBox);
    setLayout(layout);

    setWindowTitle("Resource Editor");
    resize(640, 480);

    connect(ls.get(), SIGNAL(animationAdded(AnimationHandle, QString, QString)),
            this, SLOT(animationAdded(AnimationHandle, QString, QString)));
    connect(ls.get(), SIGNAL(animationDeleted(AnimationHandle)),
            this, SLOT(animationDeleted(AnimationHandle)));
}

void ResourceEditorDialog::setupAnimationsTab()
{
    QVBoxLayout *layout = new QVBoxLayout;

    animationsList = new QListWidget;
    animationsList->setViewMode(QListView::IconMode);
    animationsList->setMovement(QListView::Static);
    animationsList->setSpacing(24);
    animationsList->setResizeMode(QListView::Adjust);
    animationsList->setSelectionMode(QAbstractItemView::ExtendedSelection);

    QHash<quint32, QString> anims = ls->getAnimations();
    for(QHash<quint32, QString>::const_iterator it = anims.constBegin();
            it != anims.constEnd(); ++it)
        animationAdded(it.key(), it.value(), ls->getAnimationPath(it.value()));

    layout->addWidget(animationsList);

    QHBoxLayout *buttonLayout = new QHBoxLayout;

    QPushButton *deleteAnimsButton =
        new QPushButton(QIcon::fromTheme("edit-delete"), "Delete");
    connect(deleteAnimsButton, SIGNAL(clicked()),
            this, SLOT(deleteAnimations()));
    buttonLayout->addWidget(deleteAnimsButton);

    QPushButton *addAnimsButton =
        new QPushButton(QIcon::fromTheme("add"), "Add Animations");
    connect(addAnimsButton, SIGNAL(clicked()),
            this, SLOT(addAnimations()));
    buttonLayout->addWidget(addAnimsButton);

    QWidget *buttons = new QWidget;
    buttons->setLayout(buttonLayout);
    layout->addWidget(buttons);

    QWidget *tab = new QWidget;
    tab->setLayout(layout);

    tabWidget->addTab(tab, "Animations");
}

void ResourceEditorDialog::deleteAnimations()
{
    QList<QListWidgetItem *> items = animationsList->selectedItems();
    QString errs("Unable to delete all animations:\n");
    bool success = true;

    for(QList<QListWidgetItem *>::const_iterator it = items.constBegin();
            it != items.constEnd(); ++it){
        QListWidgetItem *item = *it;

        AnimationHandle id = item->data(Qt::UserRole).toUInt();
        success = ls->tryDeleteAnimation(id, &errs) && success;
    }

    if(!success)
        emit error(errs);
}

void ResourceEditorDialog::chunk(const QString &filename, const QImage &image)
{
    int ppu = Renderer::PixelsPerUnit;
    QRect rect(0, image.height() - ppu, ppu, ppu);

    QFileInfo info(filename);
    QString base = info.baseName();

    TileGrid grid;
    grid.name = base;
    grid.size.rwidth() = image.width() / ppu;
    grid.size.rheight() = image.height() / ppu;
    grid.grid.reset(
            new ConstTilePtr[grid.size.rwidth() * grid.size.rheight()]);

    int i = 0;
    while(rect.y() >= 0){
        while(rect.x() < image.width()){
            QString tmplate = QDir::tempPath() + "/" + base + "_" +
                QString::number(rect.x() / ppu) + "x" +
                QString::number(rect.y() / ppu) + "_XXXXXX.png";
            QTemporaryFile tmp;
            tmp.setFileTemplate(tmplate);
            tmp.open();

            QImage chunk = image.copy(rect);
            chunk.save(&tmp);
            rect.translate(ppu, 0);
            AnimationHandle anim = ls->addNewAnimation(tmp.fileName());
            grid.grid[i] = Tile::Create(QPointF(0, 0), Tile::Type::Ignore,
                    anim);
            ++i;
        }
        rect.moveLeft(0);
        rect.translate(0, -ppu);
    }

    ls->addTileGrid(grid);
}

void ResourceEditorDialog::addAnimations()
{
    QStringList newFiles = QFileDialog::getOpenFileNames(this,
            "Select Images");

    bool yestoall = false;
    for(QStringList::const_iterator file = newFiles.constBegin();
            file != newFiles.constEnd(); ++file){
        QImage image(*file);
        if(image.width() % Renderer::PixelsPerUnit != 0 ||
                image.height() % Renderer::PixelsPerUnit != 0){
            QMessageBox::StandardButtons flags = QMessageBox::Abort;
            if(file + 1 != newFiles.constEnd())
                flags |= QMessageBox::Ignore;
            QMessageBox msg(QMessageBox::Critical, "Error adding animation",
                    *file + ": Invalid image size (" +
                    QString::number(image.width()) + ", " +
                    QString::number(image.height()) +
                    "). Must be multiple of " +
                    QString::number(Renderer::PixelsPerUnit) + ".",
                    flags, this);
            if(flags & QMessageBox::Ignore)
                msg.setDefaultButton(QMessageBox::Ignore);
            int ret = msg.exec();
            if(ret == QMessageBox::Abort)
                return;
        }else if(image.width() > Renderer::PixelsPerUnit ||
                image.height() > Renderer::PixelsPerUnit){
            if(!yestoall){
                QMessageBox::StandardButtons flags = QMessageBox::Abort |
                    QMessageBox::Ok;
                if(file + 1 != newFiles.constEnd())
                    flags |= QMessageBox::Ignore |
                        QMessageBox::YesToAll;
                QMessageBox msg(QMessageBox::Question,
                        "Large image",
                        *file + ": Large image (" +
                        QString::number(image.width()) + ", " +
                        QString::number(image.height()) +
                        ") will be chunked. ", flags, this);
                msg.setDefaultButton(QMessageBox::Ok);
                int ret = msg.exec();
                if(ret == QMessageBox::Abort)
                    return;
                if(ret == QMessageBox::Ignore)
                    continue;
                if(ret == QMessageBox::YesToAll)
                    yestoall = true;
            }
            chunk(*file, image);
        }else
            ls->addNewAnimation(*file);
    }
}

void ResourceEditorDialog::animationAdded(AnimationHandle id, QString name,
        QString path)
{
    QListWidgetItem *item = new QListWidgetItem(animationsList);
    item->setIcon(QIcon(path));
    item->setText(name);
    item->setData(Qt::UserRole, QVariant(id));
    item->setTextAlignment(Qt::AlignHCenter);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    animItems[id] = item;
}

void ResourceEditorDialog::animationDeleted(AnimationHandle id)
{
    QListWidgetItem *item = animItems.take(id);
    delete item;
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Editor ed(argv[1]);
    ed.show();

    return app.exec();
}
