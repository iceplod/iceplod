/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
LevelSet Directory Layout:

levels/
  |
  |-Mimiga_Village/
  |  |-Manifest.yaml
  |  |
  |  |-Maps/
  |  |  |-Main.ipm
  |  |  |-ArthursHouse.ipm
  |  |  `-FishingHole.ipm
  |  |
  |  `-Animations/
  |     |-down.png
  |     |-solid.png
  |     `-up.png
  |
  |-Grasstown/
  |  |
  |  ...
  |
  `-Hermit/
     |
     ...


Manifest example:

Mimiga_Village/Manifest.yaml:
%YAML 1.2
---
version: 1
maps:
    - id: main
      file: Main.ipm
      exits:
          - src: 0
            dst:
              levelset: Hermit
              map: corridor
              portal: 3
          - src: 1
            dst:
              map: arthur
              portal: 0
          - src: 2
            dst:
              map: fishing
              portal: 0
          - src: 3
            dst:
              levelset: GrassTown
              map: egg
              portal: 0
    - id: arthur
      file: ArthursHouse.ipm
      exits:
          - src: 0
            dst:
              map: main
              portal: 1
    - id: fishing
      file: FishingHole.ipm
      exits:
          - src: 0
            dst:
              map: main
              portal: 2
animations:
    - up.png
    - down.png
    - solid.png
start:
    map: main
    portal: 2
*/

#include "LevelSet.h"
#include "LevelSet_p.h"

#include <fstream>
#include <yaml-cpp/yaml.h>
#include <QDebug>

const quint32 LevelSet::CurrentVersion = 1;
const quint32 LevelSet::CurrentEditorInfoVersion = 1;

LevelSet::LevelSet(QString filename):
    filename(filename)
{}

void LevelSet::loadEditorInfo()
{
    try{
        std::ifstream in(filename.toStdString() + "/Editor.yaml");
        YAML::Parser parser(in);

        YAML::Node doc;
        if(!parser.GetNextDocument(doc))
            return;

        quint32 ver;
        doc["version"] >> ver;
        if(ver != CurrentEditorInfoVersion){
            emit error("Invalid Editor.yaml version: " + ver);
            return;
        }

        const YAML::Node &gridsNode = doc["tilegrids"];
        for(YAML::Iterator gridIt = gridsNode.begin();
                gridIt != gridsNode.end(); ++gridIt){
            const YAML::Node &gridNode = *gridIt;

            std::string s_name;
            gridNode["name"] >> s_name;

            TileGrid grid;
            grid.name = QString::fromStdString(s_name);

            gridNode["width"] >> grid.size.rwidth();

            const YAML::Node &tilesNode = gridNode["tiles"];
            if(tilesNode.size() == 0 ||
                    tilesNode.size() % grid.size.width() != 0){
                emit error("TileGrid \"" + grid.name +
                        "\" has invalid size (width: " +
                        QString::number(grid.size.width()) +
                        ", tiles: " + QString::number(tilesNode.size()) + ")");
                continue;
            }
            grid.size.rheight() = tilesNode.size() / grid.size.width();

            grid.grid.reset(
                    new ConstTilePtr[grid.size.width() * grid.size.height()]);

            int i = 0;
            for(YAML::Iterator tileIt = tilesNode.begin();
                    tileIt != tilesNode.end(); ++tileIt){
                const YAML::Node &tile = *tileIt;

                quint32 anim;
                tile["anim"] >> anim;

                quint32 type;
                tile["type"] >> type;

                grid.grid[i] = Tile::Create(QPointF(0, 0), Tile::Type(type),
                        anim);
                ++i;
            }

            addTileGrid(grid);
        }
    }catch(YAML::Exception e){
        emit error(QString("Editor.yaml parsing failure: ") + e.what());
        return;
    }
}

bool LevelSet::load(bool create)
{
    if(create){
        QDir dir(filename);
        if(dir.exists()){
            emit error("Directory already exists: " + filename);
            return false;
        }

        if(!dir.mkpath("Maps")){
            emit error("Error creating Maps directory: " + filename);
            return false;
        }

        if(!dir.mkpath("Animations")){
            emit error("Error creating Animations directory: " + filename);
            return false;
        }

        save();
    }

    qDebug() << "loading levelset:" << filename;

    try{
        std::ifstream in(filename.toStdString() + "/Manifest.yaml");
        YAML::Parser parser(in);

        YAML::Node doc;
        if(!parser.GetNextDocument(doc)){
            emit error("No Manifest.yaml in " + filename);
            return false;
        }

        quint32 ver;
        doc["version"] >> ver;
        if(ver != CurrentVersion){
            emit error("Invalid Manifest version: " + ver);
            return false;
        }

        const YAML::Node &mapsNode = doc["maps"];
        for(YAML::Iterator mapIt = mapsNode.begin(); mapIt != mapsNode.end();
                ++mapIt){
            const YAML::Node &map = *mapIt;
            std::string s_name, s_file;
            QString name;

            map["name"] >> s_name;
            name = QString::fromStdString(s_name);

            if(maps.contains(name)){
                emit error("Map already exists: " + name);
                return false;
            }

            addNewMap(name);

            const YAML::Node *exitsNode = map.FindValue("exits");
            if(exitsNode){
                for(YAML::Iterator exIt = exitsNode->begin();
                        exIt != exitsNode->end(); ++exIt){
                    const YAML::Node &exit = *exIt;
                    ExitData data;

                    quint32 srcPortal;
                    exit["src"] >> srcPortal;

                    const YAML::Node &dst = exit["dst"];

                    std::string dstLevelSet, dstMap;
                    const YAML::Node *dstLevelSetNode =
                        dst.FindValue("levelset");
                    if(dstLevelSetNode){
                        *dstLevelSetNode >> dstLevelSet;
                        data.levelset = QString::fromStdString(dstLevelSet);
                    }
                    const YAML::Node *dstMapNode = dst.FindValue("map");
                    if(dstMapNode){
                        *dstMapNode >> dstMap;
                        data.map = QString::fromStdString(dstMap);
                    }
                    dst["portal"] >> data.portal;

                    QHash<quint32, ExitData> &mapExits = exits[name];
                    mapExits.insert(srcPortal, data);
                }
            }
        }

        const YAML::Node &anims = doc["animations"];
        for(YAML::Iterator animIt = anims.begin(); animIt != anims.end();
                ++animIt){
            const YAML::Node &animNode = *animIt;
            quint32 id;
            std::string s_anim;
            QString anim;
            animNode["id"] >> id;
            animNode["name"] >> s_anim;
            anim = QString::fromStdString(s_anim);
            this->anims.insert(id, anim);
        }

        for(QHash<quint32, QString>::const_iterator name =
                this->anims.constBegin(); name != this->anims.constEnd();
                ++name){
            emit animationAdded(name.key(), name.value(),
                    filename + "/Animations/" + name.value() + ".png");
        }

        const YAML::Node *start = doc.FindValue("start");
        if(start){
            std::string s_smap;
            (*start)["map"] >> s_smap;
            startMap = QString::fromStdString(s_smap);

            (*start)["portal"] >> startPortal;
        }

    }catch(YAML::Exception e){
        emit error(QString("Manifest.yaml parsing failure: ") + e.what());
        return false;
    }

    loadEditorInfo();

    return true;
}

void LevelSet::save()
{
    YAML::Emitter mnfst, editor;

    try{
        mnfst << YAML::BeginMap;
        mnfst << YAML::Key << "version";
        mnfst << YAML::Value << CurrentVersion;
        mnfst << YAML::Key << "maps";
        mnfst << YAML::Value;
        mnfst << YAML::BeginSeq;

        for(QHash<QString, MapPtr>::const_iterator mapIt = maps.constBegin();
                mapIt != maps.constEnd(); ++mapIt){
            const QString &name = mapIt.key();
            MapPtr map = mapIt.value();

            map->save();

            mnfst << YAML::BeginMap;
            mnfst << YAML::Key << "name";
            mnfst << YAML::Value << name.toStdString();

            mnfst << YAML::Key << "exits";
            mnfst << YAML::Value;
            mnfst << YAML::BeginSeq;

            QHash<quint32, ExitData> mapExits = exits[name];
            for(QHash<quint32, ExitData>::const_iterator exitIt =
                    mapExits.constBegin(); exitIt != mapExits.constEnd();
                    ++exitIt){
                const quint32 &src = exitIt.key();
                const ExitData &data = exitIt.value();

                mnfst << YAML::BeginMap;
                mnfst << YAML::Key << "src";
                mnfst << YAML::Value << src;

                mnfst << YAML::Key << "dst";
                mnfst << YAML::Value;
                mnfst << YAML::BeginMap;
                if(!data.levelset.isEmpty()){
                    mnfst << YAML::Key << "levelset";
                    mnfst << YAML::Value << data.levelset.toStdString();
                }
                if(!data.map.isEmpty()){
                    mnfst << YAML::Key << "map";
                    mnfst << YAML::Value << data.map.toStdString();
                }
                mnfst << YAML::Key << "portal";
                mnfst << YAML::Value << data.portal;
                mnfst << YAML::EndMap;
                mnfst << YAML::EndMap;
            }

            mnfst << YAML::EndSeq;
            mnfst << YAML::EndMap;
        }

        mnfst << YAML::EndSeq;

        mnfst << YAML::Key << "animations";
        mnfst << YAML::Value;
        mnfst << YAML::BeginSeq;
        for(QHash<quint32, QString>::const_iterator anim = anims.constBegin();
                anim != anims.constEnd(); ++anim){
            mnfst << YAML::BeginMap;
            mnfst << YAML::Key << "id";
            mnfst << YAML::Value << anim.key();
            mnfst << YAML::Key << "name";
            mnfst << YAML::Value << anim.value().toStdString();
            mnfst << YAML::EndMap;
        }
        mnfst << YAML::EndSeq;

        if(!startMap.isEmpty()){
            mnfst << YAML::Key << "start";
            mnfst << YAML::Value << YAML::BeginMap;
            mnfst << YAML::Key << "map";
            mnfst << YAML::Value << startMap.toStdString();
            mnfst << YAML::Key << "portal";
            mnfst << YAML::Value << startPortal;
            mnfst << YAML::EndMap;
        }

        mnfst << YAML::EndMap;
    }catch(YAML::Exception e){
        emit error(QString("YAML exception: ") + e.what());
        return;
    }

    try{
        editor << YAML::BeginMap;
        editor << YAML::Key << "version";
        editor << YAML::Value << CurrentEditorInfoVersion;
        editor << YAML::Key << "tilegrids";
        editor << YAML::Value;
        editor << YAML::BeginSeq;

        for(QList<TileGrid>::const_iterator grid = savedCopyGrids.constBegin();
                grid != savedCopyGrids.constEnd(); ++grid){
            editor << YAML::BeginMap;
            editor << YAML::Key << "name";
            editor << YAML::Value << grid->name.toStdString();

            editor << YAML::Key << "width";
            editor << YAML::Value << grid->size.width();

            editor << YAML::Key << "tiles";
            editor << YAML::Value;
            editor << YAML::BeginSeq;

            for(unsigned int i = 0;
                    i < grid->size.width() * grid->size.height(); ++i){
                ConstTilePtr tile = grid->grid[i];

                editor << YAML::Flow;

                editor << YAML::BeginMap;

                editor << YAML::Key << "type";
                editor << YAML::Value << (quint32)tile->type;

                editor << YAML::Key << "anim";
                editor << YAML::Value << tile->anim;

                editor << YAML::EndMap;

                editor << YAML::Block;
            }

            editor << YAML::EndSeq;
            editor << YAML::EndMap;
        }

        editor << YAML::EndSeq;

        editor << YAML::EndMap;
    }catch(YAML::Exception e){
        emit error(QString("YAML exception: ") + e.what());
        return;
    }

    {
        std::ofstream file(filename.toStdString() + "/Manifest.yaml");
        file << mnfst.c_str();
    }

    {
        std::ofstream file(filename.toStdString() + "/Editor.yaml");
        file << editor.c_str();
    }

    while(!filesToDelete.isEmpty()){
        QString file = filesToDelete.takeFirst();
        qDebug() << "Deleting" << file;
        if(!QFile::remove(file))
            emit error(QString("Unable to delete: ") + file);
    }
}

void LevelSet::addNewMap(QString name)
{
    if(maps.contains(name))
        return;

    MapPtr p(new Map(filename + "/Maps/" + name));
    connect(p.get(), SIGNAL(error(QString)), this, SIGNAL(error(QString)));
    connect(p.get(), SIGNAL(portalDeleted(QString, quint32)),
            this, SLOT(mapDeletedPortal(QString, quint32)));
    p->load();
    maps.insert(name, p);

    emit mapAdded(name);
}

void LevelSet::deleteMap(QString name)
{
    maps.remove(name);

    emit mapDeleted(name);
}

MapPtr LevelSet::getMap(QString name)
{
    if(maps.contains(name))
        return maps[name];
    return MapPtr();
}

MapPtr LevelSet::getStartMap()
{
    if(startMap.isEmpty())
        return MapPtr();
    return getMap(startMap);
}

QPointF LevelSet::getStartPos()
{
    MapPtr map = getStartMap();
    if(!map)
        return QPointF(0, 0);
    QPointF ret = map->getPortalLoc(startPortal);
    ret.rx() += 0.5f;
    return ret;
}

quint32 LevelSet::getStartPortal()
{
    return startPortal;
}

QList<QString> LevelSet::getMapNames() const
{
    return maps.keys();
}

QHash<quint32, QString> LevelSet::getAnimations() const
{
    return anims;
}

QString LevelSet::getAnimationPath(const QString &name) const
{
    return filename + "/Animations/" + name + ".png";
}

QList<TileGrid> LevelSet::getSavedCopyGrids() const
{
    return savedCopyGrids;
}

void LevelSet::setStartMap(const QString &map)
{
    startMap = map;
}

void LevelSet::setStartPortal(quint32 portal)
{
    startPortal = portal;
}

void LevelSet::mapDeletedPortal(QString name, quint32 portal)
{
    QHash<quint32, ExitData> &exitDatas = exits[name];
    exitDatas.remove(portal);
}

QAbstractTableModel *LevelSet::newPortalModel(const QString &forMap)
{
    if(!maps.contains(forMap))
        return NULL;

    MapPtr map = maps.value(forMap);

    QHash<quint32, ExitData> &mapExits = exits[forMap];
    const QList<quint32> &mapPortals = map->getPortals();
    for(QList<quint32>::const_iterator it = mapPortals.constBegin();
            it != mapPortals.constEnd(); ++it){
        /* construct an ExitData for every portal */
        mapExits[*it];
    }

    return new LSPortalModel(mapExits, startMap == forMap, startPortal);
}

void LevelSet::commitPortalModel(const QString &map,
        const QAbstractTableModel *model)
{
    QHash<quint32, ExitData> &mapExits = exits[map];
    for(int row = 0; row < model->rowCount(); ++row){
        QModelIndex idx;

        idx = model->index(row, PortalModelColumns::SrcPortal);
        quint32 srcPortal = model->data(idx).toUInt();

        ExitData &data = mapExits[srcPortal];

        idx = model->index(row, PortalModelColumns::DstLevelSet);
        data.levelset = model->data(idx).toString();

        idx = model->index(row, PortalModelColumns::DstMap);
        data.map = model->data(idx).toString();

        idx = model->index(row, PortalModelColumns::DstPortal);
        data.portal = model->data(idx).toUInt();
    }
}

AnimationHandle LevelSet::addNewAnimation(QString filename)
{
    QFileInfo info(filename);

    QString baseName = info.baseName();
    if(anims.values().contains(baseName)){
        emit error("Animation already exists!");
        return StaticAnimations::Unset;
    }

    QString newFilename = this->filename + "/Animations/" + info.fileName();

    if(!QFile::copy(filename, newFilename)){
        emit error("Failed to copy animation to " + newFilename);
        return StaticAnimations::Unset;
    }

    AnimationHandle id = anims.size();
    while(anims.contains(id) && id < StaticAnimations::MaxUser)
        ++id;
    if(id == StaticAnimations::MaxUser){
        emit error("Too many animations!");
        return StaticAnimations::Unset;
    }

    anims.insert(id, baseName);

    emit animationAdded(id, baseName, newFilename);

    return id;
}

bool LevelSet::tryDeleteAnimation(AnimationHandle id, QString *perrs)
{
    bool failed = false;

    for(QHash<QString, MapPtr>::const_iterator it = maps.constBegin();
            it != maps.constEnd(); ++it){
        MapPtr map = it.value();
        QPointF loc = map->findTileWithAnimation(id);
        if(loc.x() >= 0 || loc.y() >= 0){
            QString err = anims[id] + ": In use on map \"" + it.key() +
                "\" around (" + QString::number(loc.x()) + ", " +
                QString::number(loc.y()) + ")\n";

            if(perrs){
                QString &errs = *perrs;
                errs += err;
            }else if(!failed)
                emit error(err);

            failed = true;
        }
    }

    if(failed)
        return false;

    deleteAnimation(id);

    return true;
}

void LevelSet::deleteAnimation(AnimationHandle id)
{
    filesToDelete.push_back(getAnimationPath(anims[id]));

    anims.remove(id);

    emit animationDeleted(id);
}

void LevelSet::toolCreatedTileGrid(const TileGrid &grid)
{
    savedCopyGrids.push_back(grid);
}

void LevelSet::addTileGrid(const TileGrid &grid)
{
    savedCopyGrids.push_back(grid);
    emit tileGridCreated(grid);
}

void LevelSet::createPortalTriggers()
{
    for(QHash<QString, MapPtr>::const_iterator mapIt = maps.constBegin();
            mapIt != maps.constEnd(); ++mapIt){
        QString name = mapIt.key();
        MapPtr map = mapIt.value();

        const QHash<quint32, ExitData> &mapExits = exits.value(name);
        for(QHash<quint32, ExitData>::const_iterator exit =
                mapExits.constBegin(); exit != mapExits.constEnd(); ++exit){
            map->createPortalTrigger(exit.key(), exit.value());
        }
    }
}

QString LevelSet::getName() const
{
    QDir info(filename);
    return info.dirName();
}

LSPortalModel::LSPortalModel(const QHash<quint32, ExitData> &portals,
        bool isStartMap, quint32 startPortal, QObject *parent):
    QAbstractTableModel(parent),
    portals(portals),
    isStartMap(isStartMap),
    startPortal(startPortal)
{}

int LSPortalModel::rowCount(const QModelIndex &parent) const
{
    /* as required by QAbstractItemModel::rowCount documentation */
    if(parent.isValid())
        return 0;

    return portals.size();
}

int LSPortalModel::columnCount(const QModelIndex &parent) const
{
    /* as required by QAbstractItemModel::columnCount documentation */
    if(parent.isValid())
        return 0;

    return LevelSet::PortalModelColumns::NumColumns;
}

QVariant LSPortalModel::data(const QModelIndex &index, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    const QList<quint32> &keys = portals.keys();
    if(index.row() < 0 || index.row() > keys.size())
        return QVariant();

    quint32 srcPortal = keys[index.row()];
    const ExitData &data = portals.value(srcPortal);

    switch(index.column()){
    case LevelSet::PortalModelColumns::Summary:
    {
        QString ret = QString::number(srcPortal);
        ret += " -> (";
        if(!data.levelset.isEmpty())
            ret += data.levelset + " ";
        if(!data.map.isEmpty())
            ret += data.map + " ";
        if(data.portal != 0)
            ret += QString::number(data.portal);
        ret += ")";
        if(isStartMap && srcPortal == startPortal)
            ret += " [S]";
        return QVariant(ret);
    }
    case LevelSet::PortalModelColumns::SrcPortal:
        return QVariant(srcPortal);
    case LevelSet::PortalModelColumns::DstLevelSet:
        return QVariant(data.levelset);
    case LevelSet::PortalModelColumns::DstMap:
        return QVariant(data.map);
    case LevelSet::PortalModelColumns::DstPortal:
        return QVariant(data.portal);
    }

    return QVariant();
}

bool LSPortalModel::setData(const QModelIndex &index, const QVariant &value,
        int role)
{
    if(role != Qt::EditRole)
        return false;

    const QList<quint32> &keys = portals.keys();
    if(index.row() < 0 || index.row() > keys.size())
        return false;

    quint32 srcPortal = keys[index.row()];
    ExitData &data = portals[srcPortal];

    switch(index.column()){
    case LevelSet::PortalModelColumns::SrcPortal:
        return false;
    case LevelSet::PortalModelColumns::DstLevelSet:
        data.levelset = value.toString();
        emit dataChanged(index, index);
        return true;
    case LevelSet::PortalModelColumns::DstMap:
        data.map = value.toString();
        emit dataChanged(index, index);
        return true;
    case LevelSet::PortalModelColumns::DstPortal:
        data.portal = value.toUInt();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

Qt::ItemFlags LSPortalModel::flags() const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}
