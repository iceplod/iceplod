/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "Animation.h"
#include "Math.h"

#include <QMutex>

struct TextItem {
    QPointF pos;
    Rect<float> size;
    QString text;
};

struct DialogInfo {
    QString text;
    QList<QString> choices;
    int selected;
};

class GameState {
    public:
        enum class Mode : int {
            Landscape
        };

        GameState(Mode mode = Mode::Landscape):
            mode(mode),
            zoom(1)
        {}

        Mode mode;
        QPointF cameraFocus;
        AnimationStateList layerN2Anims;
        AnimationStateList layerN1Anims;
        AnimationStateList layer0Anims;
        AnimationStateList layer1Anims;
        AnimationStateList layer2Anims;
        AnimationStateList layer3Anims;
        LineList lines;
        QList<TextItem> texts;
        DialogInfo dialog;
        float zoom;

        static GameState GetGameState()
        {
            QMutexLocker lk(&lock);
            return curState;
        }

        static void SetGameState(const GameState &gs)
        {
            QMutexLocker lk(&lock);
            curState = gs;
        }

    protected:
        static GameState curState;
        static QMutex lock;
};

#endif
