/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVELSET_H
#define LEVELSET_H

#include "ExitData.h"
#include "Map.h"

#include <QHash>
#include <QString>

class LevelSet : public QObject {
    Q_OBJECT

    public:
        LevelSet(QString filename);

        bool load(bool create = false);
        void loadEditorInfo();
        void save();

        QString getName() const;
        QString getFilename() const { return filename; }

        MapPtr getMap(QString name);
        QList<QString> getMapNames() const;

        MapPtr getStartMap();
        void setStartMap(const QString &map);

        quint32 getStartPortal();
        void setStartPortal(quint32 portal);

        QPointF getStartPos();

        QHash<quint32, QString> getAnimations() const;
        QString getAnimationPath(const QString &name) const;

        QList<TileGrid> getSavedCopyGrids() const;

        void createPortalTriggers();

        /* caller is responsible for freeing */
        QAbstractTableModel *newPortalModel(const QString &map);
        void commitPortalModel(const QString &map,
                const QAbstractTableModel *model);

        bool tryDeleteAnimation(AnimationHandle id, QString *error);

        void addTileGrid(const TileGrid &grid);

        enum PortalModelColumns : int {
            Summary = 0,
            SrcPortal,
            DstLevelSet,
            DstMap,
            DstPortal,
            NumColumns
        };

    public slots:
        void addNewMap(QString name);
        void deleteMap(QString name);

        AnimationHandle addNewAnimation(QString name);
        void deleteAnimation(AnimationHandle id);

        void mapDeletedPortal(QString name, quint32 portal);
        void toolCreatedTileGrid(const TileGrid &grid);

    signals:
        void error(QString str);

        void mapAdded(QString name);
        void mapDeleted(QString name);

        void animationAdded(AnimationHandle id, QString name, QString path);
        void animationDeleted(AnimationHandle id);

        void tileGridCreated(const TileGrid &grid);

    protected:
        QString filename;
        QHash<QString, MapPtr> maps;
        QHash<QString, QHash<quint32, ExitData> > exits;
        QHash<quint32, QString> anims;
        QList<TileGrid> savedCopyGrids;
        QString startMap;
        QStringList filesToDelete;
        quint32 startPortal;

        static const quint32 CurrentVersion;
        static const quint32 CurrentEditorInfoVersion;
};

typedef boost::shared_ptr<LevelSet> LevelSetPtr;

#endif
