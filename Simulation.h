/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMULATION_H
#define SIMULATION_H

#include <QThread>

#include "LevelSet.h"
#include "Enemy.h"
#include "Map.h"
#include "Player.h"

class SimulationThread : public QThread {
    Q_OBJECT

    public:
        SimulationThread(QString filename, QObject *parent = NULL);

    signals:
        void error(QString str);

        void animationAdded(AnimationHandle id, QString name, QString path);

    protected:
        void run();

        QString filename;
};

class Simulation : public QObject {
    Q_OBJECT

    public:
        Simulation(SimulationThread &errSink, QString filename);
        ~Simulation();

    public slots:
        void update();

    signals:
        void error(QString str);

        void animationAdded(AnimationHandle id, QString name, QString path);

    protected:
        QPointF cameraPos;
        LevelSet levelset;
        MapPtr map;
        Player player;
        DialogTreePtr dialog;
        QList<QString> forcedTriggers;
        QList<EnemyPtr> enemies;
        QHash<QString, int> vars;

        QTime lastTime;
        float accumMS;

        QPointF updateCameraPos();
};

#endif
