/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATH_H
#define MATH_H

#include <cmath>

#include <QList>
#include <QPoint>
#include <QtDebug>

#define DEG_TO_RAD(x) (x * M_PI / 180.f)
#define RAD_TO_DEG(x) (x * 180.f / M_PI)

class Math {
    public:
        static float NormalizeAngle(float angle)
        {
            while(angle < 0){
                angle += 360;
            }
            while(angle >= 360){
                angle -= 360;
            }
            return angle;
        }

        static float AngleOf(const QPointF &vector)
        {
            return NormalizeAngle(RAD_TO_DEG(atan2f(vector.y(), vector.x())));
        }

        static float Clamp(float n, float min, float max)
        {
            if(n < min){
                return min;
            }
            if(n > max){
                return max;
            }
            return n;
        }
};

template<class T = float>
struct Rect {
    public:
        Rect(T w, T h):
            w(w),
            hw(w/2.f),
            h(h),
            hh(h/2.f)
        {}

        Rect():
            w(0),
            hw(0),
            h(0),
            hh(0)
        {}

        void set(T w, T h)
        {
            this->w = w;
            this->hw = w / 2.f;
            this->h = h;
            this->hh = h / 2.f;
        }

        T w, h;
        float hw, hh;
};

template<class T>
QDebug operator<< (QDebug dbg, const Rect<T> &rec)
{
    dbg.nospace() << "[" << rec.w << ", " << rec.h << "]";
    return dbg.space();
}

struct Line {
    static const int Style_Thick  = 0x01;
    static const int Style_Thin   = 0x02;
    static const int Style_Solid  = 0x10;
    static const int Style_Dashed = 0x20;

    static const int Color_Orange = 0;
    static const int Color_Yellow = 1;

    Line(QPointF p1, QPointF p2, int style = Style_Thin | Style_Solid,
            int color = Color_Orange):
        p1(p1),
        p2(p2),
        style(style),
        color(color)
    {}

    QPointF p1, p2;
    int style;
    int color;
};

typedef QList<Line> LineList;

typedef QList<QPointF> QPointFList;

uint qHash(const QPointF &pnt);

#endif
