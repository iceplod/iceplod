/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ANIMATION_H
#define ANIMATION_H

#include "Math.h"

#include <boost/shared_ptr.hpp>

#include <QList>
#include <QString>
#include <QtOpenGL>

typedef quint32 AnimationHandle;
enum StaticAnimations : AnimationHandle {
    MaxUser = 0xFFFFFF,
    Nil,
    Unset,
    Portal,
    Solid,
    UpLow,
    UpHigh,
    DownLow,
    DownHigh,
    BasicGunAnim,
    BasicBulletAnim
};

struct AnimationState {
    AnimationState():
        angle(0),
        anim(StaticAnimations::Unset),
        glTexNum(0),
        animFrame(0),
        alpha(1.f)
    {}

    QPointF pos;
    Rect<> size;
    float angle;
    AnimationHandle anim;
    GLuint glTexNum;
    int animFrame;
    float alpha;
};

typedef QList<AnimationState> AnimationStateList;

class Frame {
    public:
        Frame(const QString &filename);
        ~Frame();

        GLuint texID;
        int frameLength;
        QString file;

        Rect<> size;
};

typedef boost::shared_ptr<Frame> FramePtr;
typedef QList<FramePtr> FramePtrList;

class Animation {
    public:
        Animation(const QString &filename);
        virtual ~Animation();

        virtual void draw(const AnimationState &state) const;

    protected:
        QString file;

        FramePtrList frames;
};

typedef boost::shared_ptr<Animation> AnimationPtr;
typedef QList<AnimationPtr> AnimationPtrList;

#endif
