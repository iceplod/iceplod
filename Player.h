/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "Actor.h"

#include "Weapon.h"

class Player : public Actor {
    public:
        Player();
        virtual ~Player();

        virtual void tick(const WorldState &worldState);

        virtual void getDrawInfo(GameState &gs) const;

        QPointF getPosition() const;
        void setPosition(const QPointF &to);

        QPointF getVelocity() const { return vel; }

        QPointF getHandPosition() const;

        bool actionRequested();
        void clearActionRequested();

        bool isShooting() const { return shooting; }
        bool isFacingRight() const { return facingRight; }

    protected:
        static const int MaxJumpTimer;

        bool checkBottom(const WorldState &ws);
        bool checkLeft(const WorldState &ws);
        bool checkRight(const WorldState &ws);
        bool checkTop(const WorldState &ws);

        bool canJump, requestAction, holdingUp, shooting, facingRight;
        int jumpTimer;

        /* bottom-center of the player */
        QPointF pos;

        QPointF vel;
        Rect<float> size;

        WeaponPtr weapon;
};

#endif
