/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Renderer.h"

const Rect<int> Renderer::GameViewport = Rect<int>(20, 15);
const int Renderer::PixelsPerUnit = 32;
const Rect<float> Renderer::UnitsPerDialogCharacter = Rect<float>(0.5f, 0.75f);
const int Renderer::DialogCharactersPerLine =
        (Renderer::GameViewport.w / UnitsPerDialogCharacter.w) - 4;
const int Renderer::LinesPerDialog = 4;

Renderer::Renderer(QWidget *parent):
    QGLWidget(parent)
{
    setAutoBufferSwap(true);
    setMinimumSize(GameViewport.w * PixelsPerUnit,
            GameViewport.h * PixelsPerUnit);
    setMouseTracking(true);
}

Rect<float> Renderer::tilesOnScreen() const
{
    GameState gs = GameState::GetGameState();
    float ppu = PixelsPerUnit * gs.zoom;
    return Rect<float>(width() / ppu, height() / ppu);
}

void Renderer::initializeGL()
{
    glClearColor(0.2f, 0.2f, 0.8f, 0);

    glEnable(GL_LINE_STIPPLE);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    loadStaticAnims();

    crudeFont.reset(new Frame("crude_font.png"));
}

void Renderer::loadStaticAnims()
{
    {
        AnimationPtr p(new Animation("tile_unset.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_portal.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_solid.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_up_low.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_up_high.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_down_low.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("tile_down_high.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("assets/BasicGun.png"));
        staticAnims.push_back(p);
    }
    {
        AnimationPtr p(new Animation("assets/BasicBullet.png"));
        staticAnims.push_back(p);
    }
}

void Renderer::addUserAnim(AnimationHandle id, QString name, QString filename)
{
    AnimationPtr p(new Animation(filename));
    dynamicAnims.insert(id, p);
}

void Renderer::deleteUserAnim(AnimationHandle id)
{
    dynamicAnims.remove(id);
}

void Renderer::resetUserAnims()
{
    dynamicAnims.clear();
}

void Renderer::resizeGL(int w, int h)
{
    Rect<> sz(w, h);
    glViewport(0, 0, sz.w, sz.h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    float orthoWidth = (float)sz.hw / (float)PixelsPerUnit;
    float orthoHeight = (float)sz.hh / (float)PixelsPerUnit;
    glOrtho(-orthoWidth, orthoWidth, -orthoHeight, orthoHeight, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Renderer::renderAnims(const AnimationStateList &anims)
{
    for(AnimationStateList::const_iterator it = anims.constBegin();
            it != anims.constEnd(); ++it){
        const AnimationState &anim = *it;
        AnimationPtr p;

        if(anim.anim > StaticAnimations::MaxUser){
            AnimationHandle realAnim = anim.anim -
                StaticAnimations::Nil - 1;
            if(realAnim < staticAnims.count() && staticAnims[realAnim])
                p = staticAnims[realAnim];
            else
                p = staticAnims[StaticAnimations::Unset -
                        StaticAnimations::Nil - 1];
        }else{
            if(dynamicAnims.contains(anim.anim))
                p = dynamicAnims.value(anim.anim);
            else
                p = staticAnims[StaticAnimations::Unset -
                        StaticAnimations::Nil - 1];
        }

        p->draw(anim);
    }
}

void Renderer::renderLines(const LineList &lines)
{
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisable(GL_TEXTURE_2D);
    for(LineList::const_iterator it = lines.constBegin();
            it != lines.constEnd(); ++it){
        const Line &line = *it;

        GLfloat verts[] = {
            (GLfloat)line.p1.x(), (GLfloat)line.p1.y(),
            (GLfloat)line.p2.x(), (GLfloat)line.p2.y()
        };

        if(line.style & Line::Style_Thick)
            glLineWidth(3.f);
        else
            glLineWidth(1.f);

        if(line.style & Line::Style_Dashed)
            glLineStipple(1, 0x6666);
        else
            glLineStipple(1, 0xFFFF);

        switch(line.color){
        case Line::Color_Yellow:
            glColor4f(1.f, 1.0f, 0.0f, 1.f);
            break;
        case Line::Color_Orange:
        default:
            glColor4f(1.f, 0.5f, 0.0f, 1.f);
            break;
        }

        glVertexPointer(2, GL_FLOAT, 0, verts);
        glDrawArrays(GL_LINES, 0, 2);
    }
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

void Renderer::renderText(const TextItem &text)
{
    static const float texCharWidth = 1 / 16.f;
    static const float texCharHeight = 1 / 6.f;

    glBindTexture(GL_TEXTURE_2D, crudeFont->texID);

    glPushMatrix();
    glTranslatef(text.pos.x(), text.pos.y(), 0.f);

    GLfloat verts[] = {
        -text.size.hw, -text.size.hh,
         text.size.hw, -text.size.hh,
        -text.size.hw,  text.size.hh,
         text.size.hw,  text.size.hh
    };

    QByteArray chars = text.text.toAscii();
    QPointF charPos(0, 0);
    for(int i = 0; i < chars.length(); ++i){
        char ch = chars.at(i);

        if(ch == '\n'){
            charPos.rx() = 0;
            charPos.ry() -= text.size.h * 1.5f;
            continue;
        }

        int offset;
        if(ch < ' ' || ch > '~')
            offset = '?' - ' ';
        else
            offset = ch - ' ';
        int row = offset / 16 - 6;
        int col = offset % 16;

        GLfloat texCoords[] = {
            col * texCharWidth, (row + 1) * texCharHeight,
            (col + 1) * texCharWidth, (row + 1) * texCharHeight,
            col * texCharWidth, row * texCharHeight,
            (col + 1) * texCharWidth, row * texCharHeight
        };

        glPushMatrix();
        glTranslatef(charPos.x(), charPos.y(), 0.f);
        glVertexPointer(2, GL_FLOAT, 0, verts);
        glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glPopMatrix();

        charPos.rx() += text.size.w;
    }

    glPopMatrix();
}

void Renderer::renderTexts(const QList<TextItem> &texts)
{
    for(QList<TextItem>::const_iterator it = texts.constBegin();
            it != texts.constEnd(); ++it){
        glColor3f(1.f, 1.f, 1.f);
        renderText(*it);
    }
}

void Renderer::renderDialog(const DialogInfo &dialog)
{
    const static float leftmostX = -GameViewport.w / 2.f + 2;
    const static float rightmostX = leftmostX +
        DialogCharactersPerLine * UnitsPerDialogCharacter.w;
    TextItem ti;

    ti.pos.rx() = -GameViewport.w / 2.f + 2;
    ti.pos.ry() = -GameViewport.h / 2.f + 5;

    ti.size = UnitsPerDialogCharacter;

    ti.text = dialog.text;

    glColor3f(1.f, 1.f, 1.f);
    renderText(ti);

    ti.pos.ry() += dialog.choices.count() * UnitsPerDialogCharacter.h * 1.5f;
    int n = 0;
    for(QList<QString>::const_iterator choiceIt = dialog.choices.constBegin();
            choiceIt != dialog.choices.constEnd(); ++choiceIt){
        ti.text = *choiceIt;

        ti.pos.rx() = rightmostX - ti.text.length() * UnitsPerDialogCharacter.w;

        if(n == dialog.selected)
            glColor3f(1.f, 1.f, 0.f);
        else
            glColor3f(1.f, 1.f, 1.f);
        renderText(ti);

        ti.pos.ry() -= UnitsPerDialogCharacter.h * 1.5f;
        ++n;
    }
}

void Renderer::landscapeMode(GameState &gs)
{
    glLoadIdentity();
    glPushMatrix();
    glTranslatef(0.5f, 0.5f, 0.f);
    glScalef(gs.zoom, gs.zoom, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
    glTranslatef(-gs.cameraFocus.x(), -gs.cameraFocus.y(), 0.f);
    renderAnims(gs.layerN2Anims);
    renderAnims(gs.layerN1Anims);
    renderAnims(gs.layer0Anims);
    renderAnims(gs.layer1Anims);
    renderAnims(gs.layer2Anims);
    renderAnims(gs.layer3Anims);
    renderLines(gs.lines);
    renderTexts(gs.texts);
    glPopMatrix();

    renderDialog(gs.dialog);
}

void Renderer::paintGL()
{
    GameState gameState = GameState::GetGameState();

    switch(gameState.mode){
        case GameState::Mode::Landscape:
            landscapeMode(gameState);
            break;
        default:
            qCritical() << "Unknown GameState mode:" << (int)gameState.mode;
            break;
    }
}

QPointF Renderer::mapToGame(const QPoint &pos) const
{
    GameState gs = GameState::GetGameState();
    float ppu = PixelsPerUnit * gs.zoom;

    return QPointF(pos.x() / ppu - 0.5f,
            (size().height() - pos.y() + 1) / ppu - 0.5f);
}

QPointF Renderer::mapFromParentToGame(const QPoint &pos) const
{
    return mapToGame(mapFromParent(pos));
}

void Renderer::mouseMoveEvent(QMouseEvent *event)
{
    emit mouseMove(event);
}
