/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLDOCKCONTENTS_P_H
#define TOOLDOCKCONTENTS_P_H

#include "ToolDockContents.h"

class ToolContents : public QWidget {
    Q_OBJECT

    public:
        ToolContents(LevelSetPtr ls, QWidget *parent = NULL):
            QWidget(parent),
            ls(ls)
        {}

        virtual void emitTool() = 0;
        virtual QString getToolName() = 0;

        virtual void reset()
        {
            this->ls.reset();
        }

        virtual void hookupLevelSet(LevelSetPtr ls)
        {
            this->ls = ls;
        }

    signals:
        void newTool(Tool *tool);

    protected:
        LevelSetPtr ls;
};

class BrushTool : public Tool {
    public:
        BrushTool():
            type(Tile::Type::Solid),
            anim(StaticAnimations::Unset)
        {}

        virtual void work(Map &map, const QPointF &pos,
                Qt::MouseButtons buttons, bool dragged);

        virtual void getDrawInfo(GameState &gs, const QPointF &mouse);

        Tile::Type type;
        AnimationHandle anim;
        bool typeEnabled;
        bool animEnabled;
};

class BrushToolContents : public ToolContents {
    Q_OBJECT

    public:
        BrushToolContents(LevelSetPtr ls, QWidget *parent = NULL);
        ~BrushToolContents();

        virtual void emitTool();
        virtual QString getToolName() { return QString("Brush"); }

        virtual void hookupLevelSet(LevelSetPtr ls);

    signals:
        void newAnimationAdded(QString filename);

    protected slots:
        void typeGroupToggled(bool on);
        void animGroupToggled(bool on);

        void upLowButton();
        void upHighButton();
        void solidButton();
        void downLowButton();
        void downHighButton();

        void animSelected(int id);

        void addAnimation(AnimationHandle id, const QString &name,
                const QString &filename);
        void deleteAnimation(AnimationHandle anim);

    protected:
        static Tile::Type storedType;
        static bool storedTypeEnabled;
        static bool storedAnimEnabled;

        BrushTool tool;

        QPushButton *upLowBut, *upHighBut, *solidBut, *downLowBut, *downHighBut;
        QComboBox *animsCombo;

    private:
        void setupPhysicsSection();
        void setupAnimationsSection();
};

class PortalTool : public Tool {
    public:
        virtual void work(Map &map, const QPointF &pos,
                Qt::MouseButtons buttons, bool dragged);
};

class PortalToolContents : public ToolContents {
    Q_OBJECT

    public:
        PortalToolContents(LevelSetPtr ls, QWidget *parent = NULL);

        virtual void emitTool();
        virtual QString getToolName(){ return QString("Place Portal"); }

    protected:
        PortalTool tool;
};

class CopyTool : public Tool {
    Q_OBJECT

    public:
        CopyTool();

        virtual void work(Map &map, const QPointF &pos,
                Qt::MouseButtons buttons, bool dragged);

        virtual void getDrawInfo(GameState &gs, const QPointF &mouse);

        void setGrid(const TileGrid &grid);
        ConstTileArray getGrid() const { return tileGrid; }
        QSize getGridSize() const;

    signals:
        void selectionStarted();

    protected:
        QPointF start, end;
        bool dragging, showBounds;
        ConstTileArray tileGrid;
};

class CopyToolContents : public ToolContents {
    Q_OBJECT

    public:
        CopyToolContents(LevelSetPtr ls, QWidget *parent = NULL);

        virtual void emitTool();
        virtual QString getToolName(){ return QString("Copy Section"); }

        virtual void hookupLevelSet(LevelSetPtr ls);

    protected slots:
        void gridSelected(int id);
        void addTileGrid(const TileGrid &grid);
        void saveButtonPushed();

        void toolSelectionStarted();

    signals:
        void tileGridCreated(const TileGrid &grid);

    protected:
        CopyTool tool;
        QHash<quint32, TileGrid> savedGrids;
        quint32 counter;

        QComboBox *savedGridsCombo;
        QLineEdit *saveName;
        QPushButton *saveButton;
};

class EnemyTool : public Tool {
    public:
        EnemyTool():
            type(Enemy::Type::FirstEnemy)
        {}

        virtual void work(Map &map, const QPointF &pos,
                Qt::MouseButtons buttons, bool dragged);

        virtual void getDrawInfo(GameState &gs, const QPointF &mouse);

        Enemy::Type type;
};

class EnemyToolContents : public ToolContents {
    Q_OBJECT

    public:
        EnemyToolContents(LevelSetPtr ls, QWidget *parent = NULL);
        virtual ~EnemyToolContents();

        virtual void emitTool();
        virtual QString getToolName() { return QString("Place Enemy"); }

    protected slots:
        void enemySelected(int id);

    protected:
        static Enemy::Type storedType;

        EnemyTool tool;

        QComboBox *enemyCombo;
};

#endif
