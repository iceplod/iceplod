/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRIGGER_H
#define TRIGGER_H

#include <QObject>
#include <QList>
#include <boost/shared_ptr.hpp>
#include <yaml-cpp/yaml.h>

struct WorldState;

class Condition;
typedef boost::shared_ptr<Condition> ConditionPtr;

class Condition {
    public:
        static ConditionPtr CreateFromNode(const YAML::Node &node);

        virtual bool isMet(const WorldState &world) = 0;
};

class Action;
typedef boost::shared_ptr<Action> ActionPtr;

class Action {
    public:
        static ActionPtr CreateFromNode(const YAML::Node &node);

        virtual bool execute(WorldState &world) = 0;
};

class Trigger {
    public:
        static Trigger CreateFromNode(const YAML::Node &node);

        bool evaluate(WorldState &world);
        void force(WorldState &world);

        QString setName(const QString &name);
        const QString &getName() { return name; }

        void addCondition(ConditionPtr cond);
        void addAction(ActionPtr action);

    protected:
        QString name;

        QList<ConditionPtr> conditions;
        QList<ActionPtr> actions;
};

#endif
