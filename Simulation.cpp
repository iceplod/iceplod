/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Simulation.h"

#include "Constants.h"
#include "Player.h"
#include "Renderer.h"
#include "WorldState.h"

SimulationThread::SimulationThread(QString filename, QObject *parent):
    QThread(parent),
    filename(filename)
{}

void SimulationThread::run()
{
    Simulation sim(*this, filename);

    exec();
}

Simulation::Simulation(SimulationThread &simThread, QString filename):
    levelset(filename),
    accumMS(0)
{
    connect(this, SIGNAL(error(QString)), &simThread, SIGNAL(error(QString)),
            Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(animationAdded(AnimationHandle, QString, QString)),
            &simThread,
            SIGNAL(animationAdded(AnimationHandle, QString, QString)),
            Qt::BlockingQueuedConnection);

    connect(&levelset, SIGNAL(error(QString)), this, SIGNAL(error(QString)));
    connect(&levelset,
            SIGNAL(animationAdded(AnimationHandle, QString, QString)),
            this,
            SIGNAL(animationAdded(AnimationHandle, QString, QString)));

    if(!levelset.load())
        return;

    levelset.createPortalTriggers();

    map = levelset.getStartMap();
    if(!map){
        emit error("LevelSet " + filename + " doesn't have a valid start "
               "location.");
        return;
    }

    map->createInitialEnemies(enemies);

    QPointF startPos = levelset.getStartPos();
    player.setPosition(startPos);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(16.6);

    lastTime = QTime::currentTime();
}

Simulation::~Simulation()
{}

QPointF Simulation::updateCameraPos()
{
    const QPointF &playerPos = player.getPosition();
    const QPointF &playerVel = player.getVelocity();

    QPointF targetPos(playerPos.x() + playerVel.x() * 40, playerPos.y());

    cameraPos.rx() += (targetPos.x() - cameraPos.x()) * 0.04f;
    cameraPos.ry() += (targetPos.y() - cameraPos.y()) * 0.6f;

    return cameraPos;
}

void Simulation::update()
{
    QTime nowTime = QTime::currentTime();
    accumMS += lastTime.msecsTo(nowTime);
    accumMS = std::min(accumMS, 3 * MS_PER_FRAME);
    lastTime = nowTime;

    while(accumMS > MS_PER_FRAME){
        if(dialog){
            if(!dialog->updateState(forcedTriggers))
                dialog.reset();
        }else{
            WorldState worldState;
            worldState.player = &player;
            worldState.map = map.get();
            worldState.levelset = &levelset;
            worldState.vars = &vars;

            player.tick(worldState);

            for(QList<EnemyPtr>::const_iterator enemy = enemies.constBegin();
                    enemy != enemies.constEnd(); ++enemy)
                (*enemy)->tick(worldState);

            while(!forcedTriggers.isEmpty()){
                QString trigger = forcedTriggers.takeFirst();
                map->forceTrigger(trigger, worldState);
            }

            map->evaluateTriggers(worldState);
            player.clearActionRequested();

            if(worldState.dialog)
                dialog = worldState.dialog;
        }

        accumMS -= MS_PER_FRAME;
    }

    GameState gs(GameState::Mode::Landscape);

    gs.cameraFocus = updateCameraPos();

    map->getDrawInfo(gs);
    player.getDrawInfo(gs);

    for(QList<EnemyPtr>::const_iterator enemy = enemies.constBegin();
            enemy != enemies.constEnd(); ++enemy)
        (*enemy)->getDrawInfo(gs);

    if(dialog)
        dialog->getDrawInfo(gs);

    GameState::SetGameState(gs);
}
