/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDITOR_P_H
#define EDITOR_P_H

#include "LevelSet.h"
#include "Math.h"

#include <QDialog>
#include <QListWidget>
#include <QSpinBox>

class LevelSetPropertiesDialog : public QDialog {
    Q_OBJECT

    public:
        LevelSetPropertiesDialog(LevelSetPtr ls, QWidget *parent = NULL);

        QString getStartMap() const;
        quint32 getStartPortal() const;

    public slots:
        void mapIndexChanged(const QString &mapName);

    protected:
        LevelSetPtr ls;

        QComboBox *startMap, *startPortal;
};

class ResizeDialog : public QDialog {
    Q_OBJECT

    public:
        ResizeDialog(Rect<int> size, QWidget *parent = NULL);

        Rect<int> getSize();

    protected:
        QSpinBox *widthEdit, *heightEdit;
};

class EditPortalsDialog : public QDialog {
    Q_OBJECT

    public:
        EditPortalsDialog(QAbstractTableModel *model, LevelSetPtr ls,
                QWidget *parent = NULL);

    public slots:
        void portalSelected(const QModelIndex &index);

    protected:
        QAbstractTableModel *model;
        LevelSetPtr ls;
        int curRow;

        QListView *portalList;
        QLineEdit *dstPortal, *dstLevelSet;
        QComboBox *dstMap;
};

class MapDockContents : public QWidget {
    Q_OBJECT

    public:
        MapDockContents(QWidget *parent = NULL);

        void reset();
        void hookupLevelSet(LevelSetPtr ls);

    public slots:
        void newMapClicked();
        void deleteMapClicked();
        void lsMapAdded(QString name);
        void lsMapDeleted(QString name);
        void mapListDoubleClicked(QListWidgetItem *item);

    signals:
        void mapSelected(MapPtr map);

    protected:
        QListWidget *mapList;
        QPushButton *newMapButton, *deleteMapButton;
        LevelSetPtr ls;
};

class ResourceEditorDialog : public QDialog {
    Q_OBJECT

    public:
        ResourceEditorDialog(LevelSetPtr ls, QWidget *parent);

    signals:
        void error(QString msg);

    protected slots:
        void deleteAnimations();
        void addAnimations();

        void animationAdded(AnimationHandle id, QString name, QString path);
        void animationDeleted(AnimationHandle id);

    protected:
        LevelSetPtr ls;
        QHash<AnimationHandle, QListWidgetItem *> animItems;

        QTabWidget *tabWidget;
        QDialogButtonBox *buttonBox;

        QListWidget *animationsList;

        void setupAnimationsTab();
        void chunk(const QString &filename, const QImage &image);
};

#endif
