/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Player.h"

#include "WorldState.h"

const int Player::MaxJumpTimer = 20;

Player::Player():
    canJump(false),
    requestAction(false),
    shooting(false),
    size(0.9f, 1.4f),
    facingRight(true),
    jumpTimer(0)
{
    weapon = Weapon::Create(Weapon::Type::BasicGun, *this);
}

Player::~Player()
{}

bool Player::checkBottom(const WorldState &ws)
{
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(pos);
    if(tile->shoveToTop(pos, shove)){
        pos += shove;
        vel.ry() = 0;
        if(!holdingUp)
            canJump = true;
        return true;
    }

    return false;
}

bool Player::checkLeft(const WorldState &ws)
{
    QPointF mid(pos.x() - size.hw, pos.y() + 0.9f);
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(mid);
    if(tile->shoveToRight(mid, shove)){
        pos += shove;
        vel.rx() = 0;
        return true;
    }

    return false;
}

bool Player::checkRight(const WorldState &ws)
{
    QPointF mid(pos.x() + size.hw, pos.y() + 0.9f);
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(mid);
    if(tile->shoveToLeft(mid, shove)){
        pos += shove;
        vel.rx() = 0;
        return true;
    }

    return false;
}

bool Player::checkTop(const WorldState &ws)
{
    QPointF top(pos.x(), pos.y() + size.h);
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(top);
    if(tile->shoveToBottom(top, shove)){
        pos += shove;
        vel.ry() = 0;
        jumpTimer = MaxJumpTimer;
        return true;
    }

    return false;
}

void Player::tick(const WorldState &worldState)
{
    KeyState keys = KeyState::GetKeyState();

    shooting = keys.shoot;

    if(keys.action){
        KeyState::SetAction(false);
        requestAction = true;
    }

    ConstTilePtr onTile = worldState.map->getTile(QPointF(pos.x(), pos.y()-1));

    /* move the player */
    if(keys.left){
        if(onTile->type == Tile::Type::Ignore)
            vel.rx() -= 0.01f;
        else
            vel.rx() -= 0.02f;
        facingRight = false;
    }
    if(keys.right){
        if(onTile->type == Tile::Type::Ignore)
            vel.rx() += 0.01f;
        else
            vel.rx() += 0.02f;
        facingRight = true;
    }

    if(onTile->type == Tile::Type::Ignore)
        vel.rx() *= 0.95f; /* friction */
    else
        vel.rx() *= 0.9f; /* friction */

    if(keys.up){
        holdingUp = true;
        if(canJump && jumpTimer < MaxJumpTimer){
            vel.ry() = 0.17f;
            ++jumpTimer;
        }else{
            vel.ry() -= Gravity;
            jumpTimer = 0;
            canJump = false;
        }
    }else{
        vel.ry() -= Gravity;
        jumpTimer = 0;
        canJump = false;
        holdingUp = false;
    }

    if(vel.ry() < -20 * Gravity)
        vel.ry() = -20 * Gravity;

    pos += vel;
    bool moved = true;

    /* validate the new position */
    /* TODO: Should have some sort of breakout to prevent infinite loops in
     * impossible physics situations. */
    while(moved){
        moved = false;

        moved = checkBottom(worldState) || moved;
        moved = checkLeft(worldState) || moved;
        moved = checkRight(worldState) || moved;
        moved = checkTop(worldState) || moved;
    }

    weapon->tick(worldState);
}

bool Player::actionRequested()
{
    return requestAction;
}

void Player::clearActionRequested()
{
    requestAction = false;
}

void Player::getDrawInfo(GameState &gs) const
{
    AnimationState as;
    as.angle = 0;
    as.pos.rx() = pos.x();
    as.pos.ry() = pos.y() + size.hh;
    as.size = size;
    gs.layer0Anims.push_back(as);

    weapon->getDrawInfo(gs);
}

QPointF Player::getHandPosition() const
{
    return QPointF(pos.x() + (facingRight ? size.hw : -size.hw),
            pos.y() + size.hh);
}

QPointF Player::getPosition() const
{
    return pos;
}

void Player::setPosition(const QPointF &to)
{
    pos = to;
}
