/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Action.h"

#include "WorldState.h"

#include <QDebug>

ActionPtr Action::CreateFromNode(const YAML::Node &node)
{
    std::string type;

    node["type"] >> type;

    if(type.compare("go_to_portal") == 0)
        return ActionPtr(new GoToPortalAction(node));
    if(type.compare("dialog") == 0)
        return ActionPtr(new DialogAction(node));
    if(type.compare("set_var") == 0)
        return ActionPtr(new SetVarAction(node));
    if(type.compare("inc_var") == 0)
        return ActionPtr(new IncVarAction(node));
    if(type.compare("dec_var") == 0)
        return ActionPtr(new DecVarAction(node));

    throw YAML::Exception(YAML::Mark::null(), "Unknown Action type: " + type);
}

/* GoToPortalAction */
GoToPortalAction::GoToPortalAction(const quint32 &portal, const QString &map,
        const QString &levelset):
    levelset(levelset),
    map(map),
    portal(portal)
{}

GoToPortalAction::GoToPortalAction(const YAML::Node &node)
{
    node["portal"] >> portal;

    const YAML::Node *dstLevelSetNode = node.FindValue("levelset");
    if(dstLevelSetNode){
        std::string dstLevelSet;

        *dstLevelSetNode >> dstLevelSet;
        map = QString::fromStdString(dstLevelSet);
    }

    const YAML::Node *dstMapNode = node.FindValue("map");
    if(dstMapNode){
        std::string dstMap;

        *dstMapNode >> dstMap;
        map = QString::fromStdString(dstMap);
    }
}

bool GoToPortalAction::execute(WorldState &world)
{
    if(portal == 0)
        return false;

    if(levelset.isEmpty() || world.levelset->getName() == levelset){
        if(map.isEmpty() || world.map->getName() == map){
            world.player->setPosition(world.map->getPortalLoc(portal));
            return true;
        }

        /* world.simulation->loadNewMap() */
    }

    /* world.simulation->loadNewLevelset() */

    return false;
}

/* DialogAction */
DialogAction::DialogAction(const YAML::Node &node)
{
    std::string str;
    node["dialog"] >> str;
    dialog = QString::fromStdString(str);
}

bool DialogAction::execute(WorldState &world)
{
    world.dialog = world.map->getDialogTree(dialog);
    return true;
}

/* SetVarAction */
SetVarAction::SetVarAction(const YAML::Node &node)
{
    std::string str;
    node["var"] >> str;
    var = QString::fromStdString(str);

    node["to"] >> val;
}

bool SetVarAction::execute(WorldState &world)
{
    world.vars->insert(var, val);
    return true;
}

/* IncVarAction */
IncVarAction::IncVarAction(const YAML::Node &node)
{
    std::string str;
    node["var"] >> str;
    var = QString::fromStdString(str);
}

bool IncVarAction::execute(WorldState &world)
{
    int value;

    if(!world.vars->contains(var))
        value = 0;
    else
        value = world.vars->value(var);

    ++value;
    world.vars->insert(var, value);

    return true;
}

/* DecVarAction */
DecVarAction::DecVarAction(const YAML::Node &node)
{
    std::string str;
    node["var"] >> str;
    var = QString::fromStdString(str);
}

bool DecVarAction::execute(WorldState &world)
{
    int value;

    if(!world.vars->contains(var))
        value = 0;
    else
        value = world.vars->value(var);

    --value;
    world.vars->insert(var, value);

    return true;
}
