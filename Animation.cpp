/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Animation.h"

#include <QImage>
#include <QtDebug>

Frame::Frame(const QString &filename):
    file(filename)
{
    qDebug() << "Loading frame:" << filename;

    QImage img;

    if(!img.load(filename)){
        texID = 0;
        qCritical() << "Error loading frame file:" << filename;
        return;
    }

    size.set(img.size().width(), img.size().height());

    if(img.format() != QImage::Format_ARGB32)
        img = img.convertToFormat(QImage::Format_ARGB32);

    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, size.w, size.h, 0,
            GL_BGRA, GL_UNSIGNED_BYTE, img.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Frame::~Frame()
{
    qDebug() << "Destroying frame:" << file;
    glDeleteTextures(1, &texID);
}

Animation::Animation(const QString &filename):
    file(filename)
{
    qDebug() << "Loading animation:" << filename;
    FramePtr p(new Frame(filename));
    frames.push_back(p);
}

Animation::~Animation()
{
    qDebug() << "Destroying animation:" << file;
    frames.clear();
}

void Animation::draw(const AnimationState &state) const
{
    const Frame &frame = **frames.begin();

    GLfloat left, right, top, bottom;

    left = -state.size.hw;
    right = state.size.hw;
    bottom = -state.size.hh;
    top = state.size.hh;

    GLfloat verts[] = {
        left, bottom,
        right, bottom,
        left, top,
        right, top
    };

    GLfloat texs[] = {
        0, 1,
        1, 1,
        0, 0,
        1, 0
    };

    glColor4f(1.f, 1.f, 1.f, state.alpha);
    glPushMatrix();
    glTranslatef(state.pos.x(), state.pos.y(), 0);
    glRotatef(state.angle, 0, 0, 1);
    if(state.glTexNum)
        glBindTexture(GL_TEXTURE_2D, state.glTexNum);
    else
        glBindTexture(GL_TEXTURE_2D, frame.texID);
    glVertexPointer(2, GL_FLOAT, 0, verts);
    glTexCoordPointer(2, GL_FLOAT, 0, texs);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}
