/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ToolDockContents.h"

#include "ToolDockContents_p.h"

#include <QPushButton>
#include <QGridLayout>

void Tool::getDrawInfo(GameState &gs, const QPointF &mouse)
{}

ToolDockContents::ToolDockContents(QWidget *parent):
    QWidget(parent),
    toolContents(NULL),
    selectedBut(NULL)
{
    QGridLayout *layout = new QGridLayout;

    brushBut = new QPushButton(QIcon("icons/tool_brush.png"), QString());
    connect(brushBut, SIGNAL(clicked()), this, SLOT(brushToolSelected()));
    layout->addWidget(brushBut, 0, 0);

    portalBut = new QPushButton(QIcon("icons/tool_portal.png"), QString());
    connect(portalBut, SIGNAL(clicked()),
            this, SLOT(portalToolSelected()));
    layout->addWidget(portalBut, 0, 1);

    copyBut = new QPushButton(QIcon("icons/tool_copy.png"), QString());
    connect(copyBut, SIGNAL(clicked()),
            this, SLOT(copyToolSelected()));
    layout->addWidget(copyBut, 0, 2);

    enemyBut = new QPushButton(QIcon("icons/tool_enemy.png"), QString());
    connect(enemyBut, SIGNAL(clicked()),
            this, SLOT(enemyToolSelected()));
    layout->addWidget(enemyBut, 0, 3);

    setLayout(layout);

    brushToolSelected();
}

void ToolDockContents::emitTool()
{
    if(toolContents)
        toolContents->emitTool();
}

void ToolDockContents::newToolSelected(ToolContents *newContents)
{
    QGridLayout *lt = (QGridLayout*)layout();

    if(toolContents){
        lt->removeWidget(toolContents);
        delete toolContents;
    }

    toolContents = newContents;
    connect(toolContents, SIGNAL(newTool(Tool *)),
            this, SIGNAL(newTool(Tool *)));
    lt->addWidget(toolContents, 1, 0, 1, 4);
    toolContents->emitTool();
    emit toolNameChanged(toolContents->getToolName());
}

void ToolDockContents::brushToolSelected()
{
    brushBut->setDisabled(true);
    if(selectedBut)
        selectedBut->setDisabled(false);
    selectedBut = brushBut;
    newToolSelected(new BrushToolContents(ls));
}

void ToolDockContents::portalToolSelected()
{
    portalBut->setDisabled(true);
    if(selectedBut)
        selectedBut->setDisabled(false);
    selectedBut = portalBut;
    newToolSelected(new PortalToolContents(ls));
}

void ToolDockContents::copyToolSelected()
{
    copyBut->setDisabled(true);
    if(selectedBut)
        selectedBut->setDisabled(false);
    selectedBut = copyBut;
    newToolSelected(new CopyToolContents(ls));
}

void ToolDockContents::enemyToolSelected()
{
    enemyBut->setDisabled(true);
    if(selectedBut)
        selectedBut->setDisabled(false);
    selectedBut = enemyBut;
    newToolSelected(new EnemyToolContents(ls));
}

void ToolDockContents::reset()
{
    ls.reset();
    if(toolContents)
        toolContents->reset();
}

void ToolDockContents::hookupLevelSet(LevelSetPtr ls)
{
    this->ls = ls;
    if(toolContents)
        toolContents->hookupLevelSet(ls);
}

/****************
 * BrushTool
 ***************/
void BrushTool::work(Map &map, const QPointF &pos,
        Qt::MouseButtons buttons, bool dragged)
{
    if(buttons == Qt::LeftButton)
        map.paint(pos, typeEnabled ? type : Tile::Type::Nil,
                animEnabled ? anim : StaticAnimations::Nil);
    else if(buttons == Qt::RightButton)
        map.paint(pos, typeEnabled ? Tile::Type::Ignore : Tile::Type::Nil,
                animEnabled ? StaticAnimations::Unset : StaticAnimations::Nil);
}

void BrushTool::getDrawInfo(GameState &gs, const QPointF &mouse)
{
    AnimationState as;
    as.pos.rx() = mouse.x() + 0.5f;
    as.pos.ry() = mouse.y() + 0.5f;
    as.size.set(1, 1);
    as.alpha = 0.5f;
    as.anim = anim;
    gs.layer3Anims.push_back(as);
}

Tile::Type BrushToolContents::storedType = Tile::Type::Solid;
bool BrushToolContents::storedTypeEnabled = true;
bool BrushToolContents::storedAnimEnabled = true;

BrushToolContents::BrushToolContents(LevelSetPtr ls, QWidget *parent):
    ToolContents(ls, parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout;

    setLayout(mainLayout);

    setupPhysicsSection();

    setupAnimationsSection();

    mainLayout->addStretch();
    setLayout(mainLayout);

    switch(storedType){
    case Tile::Type::UpLow:
        upLowButton();
        break;
    case Tile::Type::UpHigh:
        upHighButton();
        break;
    case Tile::Type::DownLow:
        downLowButton();
        break;
    case Tile::Type::DownHigh:
        downHighButton();
        break;
    case Tile::Type::Solid:
    default:
        solidButton();
        break;
    }

    hookupLevelSet(ls);
}

BrushToolContents::~BrushToolContents()
{
    storedType = tool.type;
    storedTypeEnabled = tool.typeEnabled;
    storedAnimEnabled = tool.animEnabled;
}

void BrushToolContents::setupPhysicsSection()
{
    QGroupBox *typeGroup = new QGroupBox("Physics Type");
    typeGroup->setCheckable(true);

    QGridLayout *typeLayout = new QGridLayout;

    upLowBut = new QPushButton("Up (Low)");
    connect(upLowBut, SIGNAL(clicked()), this, SLOT(upLowButton()));
    typeLayout->addWidget(upLowBut, 0, 0, 1, 1);

    upHighBut = new QPushButton("Up (High)");
    connect(upHighBut, SIGNAL(clicked()), this, SLOT(upHighButton()));
    typeLayout->addWidget(upHighBut, 0, 1, 1, 1);

    solidBut = new QPushButton("Solid");
    connect(solidBut, SIGNAL(clicked()), this, SLOT(solidButton()));
    typeLayout->addWidget(solidBut, 1, 0, 1, 2);

    downLowBut = new QPushButton("Down (Low)");
    connect(downLowBut, SIGNAL(clicked()), this, SLOT(downLowButton()));
    typeLayout->addWidget(downLowBut, 2, 0, 1, 1);

    downHighBut = new QPushButton("Down (High)");
    connect(downHighBut, SIGNAL(clicked()), this, SLOT(downHighButton()));
    typeLayout->addWidget(downHighBut, 2, 1, 1, 1);

    typeGroup->setLayout(typeLayout);

    layout()->addWidget(typeGroup);

    typeGroup->setChecked(storedTypeEnabled);
    typeGroupToggled(storedTypeEnabled);
    connect(typeGroup, SIGNAL(toggled(bool)),
            this, SLOT(typeGroupToggled(bool)));
}

void BrushToolContents::setupAnimationsSection()
{
    QGroupBox *animGroup = new QGroupBox("Animation");
    animGroup->setCheckable(true);

    QVBoxLayout *animLayout = new QVBoxLayout;

    animsCombo = new QComboBox;
    connect(animsCombo, SIGNAL(currentIndexChanged(int)),
            this, SLOT(animSelected(int)));
    animLayout->addWidget(animsCombo);

    animGroup->setLayout(animLayout);

    layout()->addWidget(animGroup);

    animGroup->setChecked(storedAnimEnabled);
    animGroupToggled(storedAnimEnabled);
    connect(animGroup, SIGNAL(toggled(bool)),
            this, SLOT(animGroupToggled(bool)));
}

void BrushToolContents::hookupLevelSet(LevelSetPtr ls)
{
    ToolContents::hookupLevelSet(ls);

    if(!ls)
        return;

    animsCombo->clear();

    QHash<quint32, QString> anims = ls->getAnimations();
    for(QHash<quint32, QString>::const_iterator it = anims.constBegin();
            it != anims.constEnd(); ++it){
        addAnimation(it.key(), it.value(), ls->getAnimationPath(it.value()));
    }

    connect(ls.get(), SIGNAL(animationAdded(
                    AnimationHandle, QString, QString)),
            this, SLOT(addAnimation(
                    AnimationHandle, const QString &, const QString &)));
    connect(ls.get(), SIGNAL(animationDeleted(AnimationHandle)), this,
            SLOT(deleteAnimation(AnimationHandle)));
}

void BrushToolContents::addAnimation(AnimationHandle id,
        const QString &name, const QString &filename)
{
    animsCombo->addItem(QIcon(filename), name, QVariant(id));
}

void BrushToolContents::deleteAnimation(AnimationHandle anim)
{
    int idx = animsCombo->findData(QVariant(anim));

    if(idx < 0){
        qWarning() << "Unknown animation handle:" << anim;
        return;
    }

    animsCombo->removeItem(idx);
}

void BrushToolContents::typeGroupToggled(bool on)
{
    tool.typeEnabled = on;
    emitTool();
}

void BrushToolContents::animGroupToggled(bool on)
{
    tool.animEnabled = on;
    emitTool();
}

void BrushToolContents::emitTool()
{
    emit newTool(&tool);
}

void BrushToolContents::upLowButton()
{
    upLowBut->setDisabled(true);
    upHighBut->setDisabled(false);
    solidBut->setDisabled(false);
    downLowBut->setDisabled(false);
    downHighBut->setDisabled(false);
    tool.type = Tile::Type::UpLow;
    emitTool();
}

void BrushToolContents::upHighButton()
{
    upLowBut->setDisabled(false);
    upHighBut->setDisabled(true);
    solidBut->setDisabled(false);
    downLowBut->setDisabled(false);
    downHighBut->setDisabled(false);
    tool.type = Tile::Type::UpHigh;
    emitTool();
}

void BrushToolContents::solidButton()
{
    upLowBut->setDisabled(false);
    upHighBut->setDisabled(false);
    solidBut->setDisabled(true);
    downLowBut->setDisabled(false);
    downHighBut->setDisabled(false);
    tool.type = Tile::Type::Solid;
    emitTool();
}

void BrushToolContents::downLowButton()
{
    upLowBut->setDisabled(false);
    upHighBut->setDisabled(false);
    solidBut->setDisabled(false);
    downLowBut->setDisabled(true);
    downHighBut->setDisabled(false);
    tool.type = Tile::Type::DownLow;
    emitTool();
}

void BrushToolContents::downHighButton()
{
    upLowBut->setDisabled(false);
    upHighBut->setDisabled(false);
    solidBut->setDisabled(false);
    downLowBut->setDisabled(false);
    downHighBut->setDisabled(true);
    tool.type = Tile::Type::DownHigh;
    emitTool();
}

void BrushToolContents::animSelected(int id)
{
    QVariant userData = animsCombo->itemData(id);
    tool.anim = userData.toInt();
}

/****************
 * PortalTool
 ***************/
void PortalTool::work(Map &map, const QPointF &pos,
        Qt::MouseButtons buttons, bool dragged)
{
    if(buttons == Qt::LeftButton)
        map.togglePortal(pos, true);
    else if(buttons == Qt::RightButton)
        map.togglePortal(pos, false);
}

PortalToolContents::PortalToolContents(LevelSetPtr ls, QWidget *parent):
    ToolContents(ls, parent)
{}

void PortalToolContents::emitTool()
{
    emit newTool(&tool);
}

/****************
 * CopyTool
 ***************/
CopyTool::CopyTool():
    start(-1, -1),
    end(-1, -1),
    dragging(false),
    tileGrid(NULL),
    showBounds(true)
{}

QSize CopyTool::getGridSize() const
{
    return QSize(end.x() - start.x() + 1, end.y() - start.y() + 1);
}

void CopyTool::getDrawInfo(GameState &gs, const QPointF &mouse)
{
    if(start.x() < 0 || start.y() < 0 || end.x() < 0 || end.y() < 0)
        return;

    if(showBounds){
        QPointF llp(end.x() < start.x() ? end.x() : start.x(),
                end.y() < start.y() ? end.y() : start.y());
        QPointF urp(end.x() > start.x() ? end.x() : start.x(),
                end.y() > start.y() ? end.y() : start.y());

        QPointF ll(llp.x(), llp.y());
        QPointF lr(urp.x() + 1, llp.y());
        QPointF ur(urp.x() + 1, urp.y() + 1);
        QPointF ul(llp.x(), urp.y() + 1);

        Line line(ll, lr, Line::Style_Thick | Line::Style_Dashed,
                Line::Color_Yellow);
        gs.lines.push_back(line);

        line.p1 = lr;
        line.p2 = ur;
        gs.lines.push_back(line);

        line.p1 = ur;
        line.p2 = ul;
        gs.lines.push_back(line);

        line.p1 = ul;
        line.p2 = ll;
        gs.lines.push_back(line);
    }

    if(!dragging && !(start.x() < 0 || start.y() < 0 || end.x() < 0 || end.y() < 0)){
        QSize size = getGridSize();

        QPointF mouseR((int)mouse.x(), (int)mouse.y());

        AnimationState as;
        as.size.set(1, 1);
        as.alpha = 0.5f;

        for(int y = 0; y < size.height(); ++y){
            for(int x = 0; x < size.width(); ++x){
                ConstTilePtr tile = tileGrid[y * size.width() + x];
                as.pos = QPointF(mouseR.x() + x + 0.5f, mouseR.y() + y + 0.5f);
                as.anim = tile->anim;
                gs.layer3Anims.push_back(as);
            }
        }
    }
}

void CopyTool::work(Map &map, const QPointF &pos,
        Qt::MouseButtons buttons, bool dragged)
{
    if(buttons == Qt::RightButton){
        if(!dragging){
            emit selectionStarted();
            showBounds = true;
            end = start = pos;
            dragging = true;
        }else{
            end = pos;
            if(!dragged)
                dragging = false;
        }
    }

    if((buttons == Qt::RightButton && !dragging) &&
            !(start.x() < 0 || start.y() < 0 || end.x() < 0 || end.y() < 0)){
        QPointF ll(end.x() < start.x() ? end.x() : start.x(),
                end.y() < start.y() ? end.y() : start.y());
        QPointF ur(end.x() > start.x() ? end.x() : start.x(),
                end.y() > start.y() ? end.y() : start.y());
        start = ll;
        end = ur;

        QSize size = getGridSize();
        tileGrid.reset(new ConstTilePtr[size.width() * size.height()]);

        for(int y = 0; y < size.height(); ++y){
            for(int x = 0; x < size.width(); ++x){
                tileGrid[y * size.width() + x] =
                    map.getTile(QPointF(start.x() + x, start.y() + y));
            }
        }
    }

    if(buttons == Qt::LeftButton && !dragging){
        if(start.x() < 0 || start.y() < 0 || end.x() < 0 || end.y() < 0)
            return;

        QSize size = getGridSize();

        for(int y = 0; y < size.height(); ++y){
            for(int x = 0; x < size.width(); ++x){
                ConstTilePtr tile = tileGrid[y * size.width() + x];
                map.paint(QPointF(pos.x() + x, pos.y() + y), tile->type,
                        tile->anim);
            }
        }
    }
}

void CopyTool::setGrid(const TileGrid &grid)
{
    dragging = false;
    showBounds = false;
    start.rx() = start.ry() = 0;
    end.rx() = grid.size.width() - 1;
    end.ry() = grid.size.height() - 1;
    tileGrid = grid.grid;
}

void CopyToolContents::addTileGrid(const TileGrid &grid)
{
    if(!grid.grid)
        savedGridsCombo->addItem("N/A", QVariant(counter));
    else{
        QString suffix = " (" +
                QString::number(grid.size.width()) + ", " +
                QString::number(grid.size.height()) + ")";
        if(grid.name.isEmpty())
            savedGridsCombo->addItem("[Unsaved]" + suffix, QVariant(counter));
        else
            savedGridsCombo->addItem(grid.name + suffix, QVariant(counter));
    }
    savedGrids.insert(counter, grid);
    ++counter;
}

void CopyToolContents::toolSelectionStarted()
{
    savedGridsCombo->setCurrentIndex(0);
}

void CopyToolContents::gridSelected(int idx)
{
    QVariant userData = savedGridsCombo->itemData(idx);
    quint32 id = userData.toInt();

    if(!savedGrids.contains(id))
        return;

    tool.setGrid(savedGrids[id]);
}

void CopyToolContents::hookupLevelSet(LevelSetPtr ls)
{
    ToolContents::hookupLevelSet(ls);

    if(!ls)
        return;

    savedGridsCombo->clear();

    /* add an "empty" grid, for the default selection */
    addTileGrid(TileGrid());

    QList<TileGrid> grids = ls->getSavedCopyGrids();

    for(QList<TileGrid>::const_iterator grid = grids.constBegin();
            grid != grids.constEnd(); ++grid)
        addTileGrid(*grid);

    connect(this, SIGNAL(tileGridCreated(const TileGrid &)),
            ls.get(), SLOT(toolCreatedTileGrid(const TileGrid &)));
    connect(ls.get(), SIGNAL(tileGridCreated(const TileGrid &)),
            this, SLOT(addTileGrid(const TileGrid &)));
}

void CopyToolContents::saveButtonPushed()
{
    TileGrid grid;

    grid.name = saveName->text();
    grid.grid = tool.getGrid();
    grid.size = tool.getGridSize();
    if(grid.size.width() <= 0 || grid.size.height() <= 0)
        return;

    addTileGrid(grid);

    if(!grid.name.isEmpty())
        emit tileGridCreated(grid);
}

CopyToolContents::CopyToolContents(LevelSetPtr ls, QWidget *parent):
    ToolContents(ls, parent),
    counter(0)
{
    QVBoxLayout *layout = new QVBoxLayout;

    QGroupBox *loadGroup = new QGroupBox("Load pattern");
    QVBoxLayout *loadLayout = new QVBoxLayout;
    savedGridsCombo = new QComboBox;
    loadLayout->addWidget(savedGridsCombo);
    loadGroup->setLayout(loadLayout);

    layout->addWidget(loadGroup);

    QGroupBox *saveGroup = new QGroupBox("Save pattern");

    QVBoxLayout *saveLayout = new QVBoxLayout;

    saveName = new QLineEdit;
    saveLayout->addWidget(saveName);

    saveButton = new QPushButton("Save");
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveButtonPushed()));
    saveLayout->addWidget(saveButton);

    saveGroup->setLayout(saveLayout);

    layout->addWidget(saveGroup);

    layout->addStretch();

    setLayout(layout);

    hookupLevelSet(ls);

    connect(savedGridsCombo, SIGNAL(currentIndexChanged(int)),
            this, SLOT(gridSelected(int)));
    connect(&tool, SIGNAL(selectionStarted()),
            this, SLOT(toolSelectionStarted()));
}

void CopyToolContents::emitTool()
{
    emit newTool(&tool);
}

/****************
 * EnemyTool
 ***************/
void EnemyTool::work(Map &map, const QPointF &pos,
        Qt::MouseButtons buttons, bool dragged)
{
    if(buttons == Qt::LeftButton)
        map.placeEnemy(pos, type);
    else if(buttons == Qt::RightButton)
        map.removeEnemy(pos);
}

static void copy_anims(AnimationStateList &dst, const AnimationStateList &src,
        float alpha_factor)
{
    for(AnimationStateList::const_iterator anim = src.constBegin();
            anim != src.constEnd(); ++anim){
        AnimationState st = *anim;
        st.alpha *= alpha_factor;
        dst.push_back(st);
    }
}

void EnemyTool::getDrawInfo(GameState &gs, const QPointF &mouse)
{
    GameState tmp_gs;

    Enemy::GetStaticDrawInfo(type,
            QPointF(mouse.x() + 0.5f, mouse.y()), tmp_gs);

    copy_anims(gs.layerN2Anims, tmp_gs.layerN2Anims, 0.5f);
    copy_anims(gs.layerN1Anims, tmp_gs.layerN1Anims, 0.5f);
    copy_anims(gs.layer0Anims, tmp_gs.layer0Anims, 0.5f);
    copy_anims(gs.layer1Anims, tmp_gs.layer1Anims, 0.5f);
    copy_anims(gs.layer2Anims, tmp_gs.layer2Anims, 0.5f);
    copy_anims(gs.layer3Anims, tmp_gs.layer3Anims, 0.5f);
}

Enemy::Type EnemyToolContents::storedType = Enemy::Type::FirstEnemy;

EnemyToolContents::EnemyToolContents(LevelSetPtr ls, QWidget *parent):
    ToolContents(ls, parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout;

    enemyCombo = new QComboBox;
    for(quint32 type = quint32(Enemy::Type::FirstEnemy);
            type < quint32(Enemy::Type::LastEnemy); ++type)
        enemyCombo->addItem(Enemy::GetEnemyName((Enemy::Type)type),
                QVariant(type));
    enemyCombo->setCurrentIndex(int(storedType));
    connect(enemyCombo, SIGNAL(currentIndexChanged(int)),
            this, SLOT(enemySelected(int)));
    mainLayout->addWidget(enemyCombo);

    mainLayout->addStretch();
    setLayout(mainLayout);
}

EnemyToolContents::~EnemyToolContents()
{
    storedType = tool.type;
}

void EnemyToolContents::emitTool()
{
    emit newTool(&tool);
}

void EnemyToolContents::enemySelected(int id)
{
    QVariant userData = enemyCombo->itemData(id);
    tool.type = (Enemy::Type)userData.toInt();
}
