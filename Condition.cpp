/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Condition.h"

#include "WorldState.h"

#include <QDebug>

ConditionPtr Condition::CreateFromNode(const YAML::Node &node)
{
    std::string type;

    node["type"] >> type;

    if(type.compare("player_position") == 0)
        return ConditionPtr(new PlayerPositionCondition(node));
    if(type.compare("player_action") == 0)
        return ConditionPtr(new PlayerActionCondition(node));
    if(type.compare("var_value") == 0)
        return ConditionPtr(new VarValueCondition(node));
    if(type.compare("false") == 0)
        return ConditionPtr(new FalseCondition(node));

    throw YAML::Exception(YAML::Mark::null(),
            "Unknown Condition type: " + type);
}

/* FalseCondition */
FalseCondition::FalseCondition(const YAML::Node &node)
{}

bool FalseCondition::isMet(const WorldState &world)
{
    return false;
}

/* PlayerActionCondition */
PlayerActionCondition::PlayerActionCondition()
{}

PlayerActionCondition::PlayerActionCondition(const YAML::Node &node)
{}

bool PlayerActionCondition::isMet(const WorldState &world)
{
    return world.player->actionRequested();
}

/* PlayerPositionCondition */
PlayerPositionCondition::PlayerPositionCondition(const QPointF &ll,
        const QPointF &ur):
    ll(ll),
    ur(ur)
{}

PlayerPositionCondition::PlayerPositionCondition(const YAML::Node &node)
{
    node["ll_x"] >> ll.rx();
    node["ll_y"] >> ll.ry();
    node["ur_x"] >> ur.rx();
    node["ur_y"] >> ur.ry();
}

bool PlayerPositionCondition::isMet(const WorldState &world)
{
    const QPointF &playerPos = world.player->getPosition();
    return (playerPos.x() > ll.x() && playerPos.x() < (ur.x() + 1) &&
            (playerPos.y() + 0.1f) > ll.y() && playerPos.y() < (ur.y() + 1));
}

/* VarValueCondition */
VarValueCondition::VarValueCondition(const YAML::Node &node)
{
    std::string str;
    node["var"] >> str;
    var = QString::fromStdString(str);

    node["op"] >> str;
    if(str == "<")
        op = Operation::Less;
    else if(str == "<=")
        op = Operation::LessEq;
    else if(str == "=")
        op = Operation::Eq;
    else if(str == ">=")
        op = Operation::GreaterEq;
    else if(str == ">")
        op = Operation::Greater;
    else
        throw YAML::Exception(YAML::Mark::null(), "Unknown operation: " + str);

    node["value"] >> val;
}

bool VarValueCondition::isMet(const WorldState &world)
{
    int value;

    if(!world.vars->contains(var))
        value = 0;
    else
        value = world.vars->value(var);

    switch(op){
        case Operation::Less:
            return value < val;
        case Operation::LessEq:
            return value <= val;
        case Operation::Eq:
            return value == val;
        case Operation::GreaterEq:
            return value >= val;
        case Operation::Greater:
            return value > val;
    }

    return false;
}
