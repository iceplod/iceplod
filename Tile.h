/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TILE_H
#define TILE_H

#include "Animation.h"
#include "GameState.h"

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

class Tile;

typedef boost::shared_ptr<const Tile> ConstTilePtr;
typedef boost::shared_array<ConstTilePtr> ConstTileArray;

struct TileGrid {
    QString name;
    ConstTileArray grid;
    QSize size;
};

class Tile {
    public:
        enum class Type : quint32 {
            Nil = 0,
            Ignore,
            Solid,
            UpLow,
            UpHigh,
            DownLow,
            DownHigh
        };

        static ConstTilePtr Create(const QPointF &pos, Type type,
                AnimationHandle anim = StaticAnimations::Unset);
        static ConstTilePtr Read(const QPointF &pos, QDataStream &in);

        void write(QDataStream &out) const;

        virtual void getDrawInfo(GameState &gs) const;
        virtual void getPhysDrawInfo(GameState &gs) const;
        virtual AnimationHandle getPhysAnim() const = 0;

        virtual bool shoveToTop(const QPointF &pt, QPointF &shove) const
            { return false; }
        virtual bool shoveToBottom(const QPointF &pt, QPointF &shove) const
            { return false; }
        virtual bool shoveToRight(const QPointF &pt, QPointF &shove) const
            { return false; }
        virtual bool shoveToLeft(const QPointF &pt, QPointF &shove) const
            { return false; }

        /* lower-left corner of the tile */
        const QPointF pos;

        const AnimationHandle anim;
        const Type type;

    protected:
        static const float ShoveThreshold;

        Tile(const QPointF &pos, Type type, AnimationHandle anim);
};

#endif
