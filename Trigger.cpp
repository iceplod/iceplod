/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Trigger.h"

#include "WorldState.h"

Trigger Trigger::CreateFromNode(const YAML::Node &node)
{
    Trigger ret;

    std::string s_name;
    node["name"] >> s_name;

    ret.setName(QString::fromStdString(s_name));

    const YAML::Node *condNode = node.FindValue("conditions");
    if(condNode){
        for(YAML::Iterator condIt = condNode->begin();
                condIt != condNode->end(); ++condIt)
            ret.addCondition(Condition::CreateFromNode(*condIt));
    }

    const YAML::Node *actNode = node.FindValue("actions");
    if(actNode){
        for(YAML::Iterator actIt = actNode->begin(); actIt != actNode->end();
                ++actIt)
            ret.addAction(Action::CreateFromNode(*actIt));
    }

    return ret;
}

QString Trigger::setName(const QString &newName)
{
    name = newName;
    return name;
}

void Trigger::addCondition(ConditionPtr cond)
{
    conditions.push_back(cond);
}

void Trigger::addAction(ActionPtr action)
{
    actions.push_back(action);
}

bool Trigger::evaluate(WorldState &world)
{
    for(QList<ConditionPtr>::const_iterator cond = conditions.constBegin();
            cond != conditions.constEnd(); ++cond){
        if(!(*cond)->isMet(world))
            return false;
    }

    force(world);

    return true;
}

void Trigger::force(WorldState &world)
{
    for(QList<ActionPtr>::const_iterator action = actions.constBegin();
            action != actions.constEnd(); ++action)
        (*action)->execute(world);
}
