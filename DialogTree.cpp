/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DialogTree.h"

#include "KeyState.h"
#include "Renderer.h"

#include <QDebug>

DialogTree::DialogTree(const YAML::Node &root):
    active(false)
{
    std::string str;
    root["name"] >> str;
    name = QString::fromStdString(str);

    const YAML::Node &dialogNode = root["dialog"];
    for(YAML::Iterator itemIt = dialogNode.begin();
            itemIt != dialogNode.end(); ++itemIt){
        DialogItemPtr p(new DialogItem(*itemIt, itemsById));
        items.push_back(p);
        if(!p->id.isEmpty()){
            if(itemsById.contains(p->id))
                throw YAML::Exception(YAML::Mark::null(),
                        "Duplicate dialog item ID: " + p->id.toStdString());
            itemsById.insert(p->id, p);
        }
    }

    QList<DialogItemPtr>::const_iterator cIt = items.constBegin();
    QList<DialogItemPtr>::const_iterator cEnd = items.constEnd();
    for(QList<DialogItemPtr>::iterator it = items.begin(); it != items.end();
            ++it){
        (*it)->root = true;
        (*it)->setupChildren(cIt, cEnd);
        ++cIt;
    }
}

DialogItem::DialogItem(const YAML::Node &root,
        QHash<QString, DialogItemPtr> &itemsById):
    root(false)
{
    const YAML::Node *node;

    node = root.FindValue("label");
    if(node){
        std::string str;
        *node >> str;
        id = QString::fromStdString(str);
    }

    node = root.FindValue("text");
    if(node){
        std::string str;
        *node >> str;
        text = QString::fromStdString(str);
    }

    node = root.FindValue("force_trigger");
    if(node){
        if(!text.isEmpty())
            throw YAML::Exception(YAML::Mark::null(),
                    "text, force_trigger, and goto are mutually exclusive");
        std::string str;
        *node >> str;
        forceTrigger = QString::fromStdString(str);
    }

    node = root.FindValue("goto");
    if(node){
        if(!text.isEmpty() || !forceTrigger.isEmpty())
            throw YAML::Exception(YAML::Mark::null(),
                    "text, force_trigger, and goto are mutually exclusive");
        std::string str;
        *node >> str;
        goTo = QString::fromStdString(str);
    }

    node = root.FindValue("options");
    if(node){
        if(!goTo.isEmpty() || !forceTrigger.isEmpty())
            throw YAML::Exception(YAML::Mark::null(),
                    "options can only be used with text items");
        for(YAML::Iterator choiceIt = node->begin(); choiceIt != node->end();
                ++choiceIt)
            options.push_back(DialogChoice(*choiceIt, itemsById));
    }
}

void DialogItem::setupChildren(QList<DialogItemPtr>::const_iterator thisItem,
        QList<DialogItemPtr>::const_iterator thisEnd)
{
    this->thisEnd = thisEnd;
    this->thisItem = thisItem;
    for(QList<DialogChoice>::iterator choice = options.begin();
            choice != options.end(); ++choice){
        QList<DialogItemPtr>::const_iterator cIt = choice->items.constBegin();
        QList<DialogItemPtr>::const_iterator cEnd = choice->items.constEnd();
        for(QList<DialogItemPtr>::iterator item = choice->items.begin();
                item != choice->items.end(); ++item){
            (*item)->parentItem = thisItem;
            (*item)->parentEnd = thisEnd;
            (*item)->setupChildren(cIt, cEnd);
            ++cIt;
        }
    }
}

DialogChoice::DialogChoice(const YAML::Node &root,
        QHash<QString, DialogItemPtr> &itemsById)
{
    std::string str;
    root["choice"] >> str;
    choice = QString::fromStdString(str);

    const YAML::Node *dialogNode = root.FindValue("dialog");
    if(dialogNode){
        for(YAML::Iterator itemIt = dialogNode->begin();
                itemIt != dialogNode->end(); ++itemIt){
            DialogItemPtr p(new DialogItem(*itemIt, itemsById));
            items.push_back(p);
            if(!p->id.isEmpty()){
                if(itemsById.contains(p->id))
                    throw YAML::Exception(YAML::Mark::null(),
                            "Duplicate dialog item ID: " + p->id.toStdString());
                itemsById.insert(p->id, p);
            }
        }
    }
}

void DialogTree::flowText()
{
    flowedText = QString();

    if(textOffs == -1)
        textOffs = 0;

    QString tail = (*curItem)->text.mid(textOffs);

    int i = 0;
    int lines = 0;
    while(i < tail.length()){
        int j = 0, l = 0;
        while(j < tail.length() - i){
            if(l + j >= Renderer::DialogCharactersPerLine){
                flowedText += QChar('\n');
                j = 0;
                l = 0;
                ++lines;
                if(lines >= Renderer::LinesPerDialog)
                    goto done;
            }else if(tail[i + j] == QChar(' ') || tail[i + j] == QChar('-')){
                flowedText += tail.mid(i, j + 1);
                i += j + 1;
                l += j + 1;
                j = 0;
            }else{
                ++j;
            }
        }
        if(j >= tail.length() - i){
            flowedText += tail.mid(i);
            i = tail.length();
        }
    }

done:
    if(i >= tail.length())
        textOffs = -1;
    else
        textOffs = i;
}

bool DialogTree::updateState(QList<QString> &forcedTriggers)
{
    static bool down_held = true;
    static bool up_held = true;

    KeyState ks = KeyState::GetKeyState();

    if(!active){
        curEnd = items.constEnd();
        curItem = items.constBegin();
        active = true;
        up_held = true;
        down_held = true;
        selection = 0;
        textOffs = -1;
        flowText();
    }

    if(ks.down){
        if(!down_held && (*curItem)->options.count() > 0 && textOffs == -1){
            ++selection;
            selection %= (*curItem)->options.count();
            down_held = true;
        }
    }else
        down_held = false;

    if(ks.up){
        if(!up_held && (*curItem)->options.count() > 0 && textOffs == -1){
            --selection;
            if(selection < 0)
                selection = (*curItem)->options.count() - 1;
            up_held = true;
        }
    }else
        up_held = false;

    if(ks.action){
        KeyState::SetAction(false);

        if(textOffs != -1){
            flowText();
            return true;
        }

        QList<DialogItemPtr>::const_iterator parentEnd, parentItem;
        bool root;
        if((*curItem)->options.count() > 0){
            root = false;
            parentEnd = curEnd;
            parentItem = curItem;
            curEnd = (*curItem)->options[selection].items.constEnd();
            curItem = (*curItem)->options[selection].items.constBegin();
        }else{
            root = (*curItem)->root;
            parentEnd = (*curItem)->parentEnd;
            parentItem = (*curItem)->parentItem;
            ++curItem;
        }

        while(1){
            while(curItem == curEnd){
                if(root){
                    active = false;
                    return false;
                }
                curEnd = parentEnd;
                curItem = parentItem;
                parentEnd = (*curItem)->parentEnd;
                parentItem  = (*curItem)->parentItem;
                root = (*curItem)->root;
                ++curItem;
            }
            selection = 0;

            if(!(*curItem)->goTo.isEmpty()){
                if(!itemsById.contains((*curItem)->goTo)){
                    if((*curItem)->goTo != "TERMINATE")
                        qCritical() << "Invalid goto target:" <<
                            (*curItem)->goTo;
                    active = false;
                    return false;
                }
                DialogItemPtr item = itemsById[(*curItem)->goTo];
                curEnd = item->thisEnd;
                curItem = item->thisItem;
                parentEnd = (*curItem)->parentEnd;
                parentItem  = (*curItem)->parentItem;
                root = (*curItem)->root;
                continue;
            }

            if(!(*curItem)->forceTrigger.isEmpty()){
                forcedTriggers.push_back((*curItem)->forceTrigger);
                ++curItem;
                continue;
            }

            break;
        }

        flowText();
    }

    return true;
}

void DialogTree::getDrawInfo(GameState &gs) const
{
    if(!active)
        return;

    gs.dialog.text = flowedText;
    gs.dialog.selected = selection;
    if(textOffs == -1)
        for(QList<DialogChoice>::const_iterator it =
                    (*curItem)->options.constBegin();
                it != (*curItem)->options.constEnd(); ++it)
            gs.dialog.choices.push_back(it->choice);
}
