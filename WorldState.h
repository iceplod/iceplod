/*
 *  Copyright 2011 Andrew Eikum
 *  Copyright 2010 Zachary Irwin
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WORLDSTATE_H
#define WORLDSTATE_H

#include "LevelSet.h"
#include "Map.h"
#include "Player.h"

struct WorldState {
    Player *player;
    Map *map;
    LevelSet *levelset;
    DialogTreePtr dialog;
    QHash<QString, int> *vars;
};

#endif
