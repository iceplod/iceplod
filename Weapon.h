/*
 *  Copyright 2011 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WEAPON_H
#define WEAPON_H

#include "Actor.h"

#include <boost/shared_ptr.hpp>
#include <QtGlobal>

class Weapon;
class Player;

typedef boost::shared_ptr<Weapon> WeaponPtr;

class Weapon : public Actor {
    public:
        enum class Type : quint32 {
            BasicGun
        };

        static WeaponPtr Create(Type type, const Player &player);

    protected:
        Weapon(const Player &player):
            player(player)
        {}

        const Player &player;
};

#endif
