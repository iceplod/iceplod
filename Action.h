/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ACTION_H
#define ACTION_H

#include "DialogTree.h"
#include "Trigger.h"

struct WorldState;

class DialogAction : public Action {
    public:
        DialogAction(const YAML::Node &node);

        virtual bool execute(WorldState &world);

    protected:
        QString dialog;
};

class GoToPortalAction : public Action {
    public:
        GoToPortalAction(const quint32 &portal, const QString &map,
                const QString &levelset);
        GoToPortalAction(const YAML::Node &node);

        virtual bool execute(WorldState &world);

        QString levelset;
        QString map;
        quint32 portal;
};

class SetVarAction : public Action {
    public:
        SetVarAction(const YAML::Node &node);

        virtual bool execute(WorldState &world);

        QString var;
        int val;
};

class IncVarAction : public Action {
    public:
        IncVarAction(const YAML::Node &node);

        virtual bool execute(WorldState &world);

        QString var;
};

class DecVarAction : public Action {
    public:
        DecVarAction(const YAML::Node &node);

        virtual bool execute(WorldState &world);

        QString var;
};

#endif
