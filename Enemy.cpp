/*
 *  Copyright 2011 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Enemy.h"

#include "Constants.h"
#include "Tile.h"
#include "WorldState.h"

class WalkerEnemy : public Enemy {
    public:
        WalkerEnemy(const QPointF &pos);

        virtual void tick(const WorldState &worldState);
        virtual void getDrawInfo(GameState &gs) const;

    protected:
        bool facingRight;
};

WalkerEnemy::WalkerEnemy(const QPointF &pos):
    Enemy(pos),
    facingRight(false)
{
    size.set(1, 1);
}

void WalkerEnemy::tick(const WorldState &worldState)
{
    if(facingRight)
        vel.rx() = 0.08f;
    else
        vel.rx() = -0.08f;

    vel.ry() -= Gravity;
    if(vel.ry() < -20 * Gravity)
        vel.ry() = -20 * Gravity;

    pos += vel;

    bool moved = true;
    /* TODO: Some breakout for infinite loops. cf Player::tick() */
    while(moved){
        moved = false;

        moved = checkBottom(worldState) || moved;
        moved = checkTop(worldState) || moved;
        if(checkLeft(worldState)){
            facingRight = true;
            moved = true;
        }
        if(checkRight(worldState)){
            facingRight = false;
            moved = true;
        }
    }
}

void WalkerEnemy::getDrawInfo(GameState &gs) const
{
    AnimationState as;
    as.pos.rx() = pos.x();
    as.pos.ry() = pos.y() + size.hh;
    if(facingRight)
        as.size.set(size.w, size.h);
    else
        as.size.set(-size.w, size.h);
    as.anim = StaticAnimations::Unset;
    gs.layer0Anims.push_back(as);
}

bool Enemy::checkBottom(const WorldState &ws)
{
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(pos);
    if(tile->shoveToTop(pos, shove)){
        pos += shove;
        vel.ry() = 0;
        return true;
    }

    return false;
}

bool Enemy::checkLeft(const WorldState &ws)
{
    bool moved = false;

    for(float y = pos.y() + 0.9f; y < pos.y() + size.h; ++y){
        QPointF mid(pos.x() - size.hw, y);
        QPointF shove;

        ConstTilePtr tile = ws.map->getTile(mid);
        if(tile->shoveToRight(mid, shove)){
            pos += shove;
            vel.rx() = 0;
            moved = true;
        }
    }

    return moved;
}

bool Enemy::checkRight(const WorldState &ws)
{
    bool moved = false;

    for(float y = pos.y() + 0.9f; y < pos.y() + size.h; ++y){
        QPointF mid(pos.x() + size.hw, y);
        QPointF shove;

        ConstTilePtr tile = ws.map->getTile(mid);
        if(tile->shoveToLeft(mid, shove)){
            pos += shove;
            vel.rx() = 0;
            moved = true;
        }
    }

    return moved;
}

bool Enemy::checkTop(const WorldState &ws)
{
    QPointF top(pos.x(), pos.y() + size.h);
    QPointF shove;

    ConstTilePtr tile = ws.map->getTile(top);
    if(tile->shoveToBottom(top, shove)){
        pos += shove;
        vel.ry() = 0;
        return true;
    }

    return false;
}

Enemy::Enemy(const QPointF &pos):
    pos(pos)
{}

EnemyPtr Enemy::Create(Enemy::Type type, const QPointF &pos)
{
    switch(type){
        case Type::Walker:
            {
                EnemyPtr p(new WalkerEnemy(pos));
                return p;
            }
        default:
            qDebug() << "Unknown enemy type:" << (int)type;
            return EnemyPtr();
    }
}

QString Enemy::GetEnemyName(Enemy::Type type)
{
    switch(type){
        case Type::Walker:
            return QString("Walker");
        default:
            return QString("MissingNo.");
    }
}

void Enemy::GetStaticDrawInfo(Enemy::Type type, QPointF pos, GameState &gs)
{
    EnemyPtr p = Create(type, pos);
    p->getDrawInfo(gs);
}
