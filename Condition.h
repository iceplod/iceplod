/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONDITION_H
#define CONDITION_H

#include "Trigger.h"

#include <QPointF>

class FalseCondition : public Condition {
    public:
        FalseCondition(const YAML::Node &node);

        virtual bool isMet(const WorldState &world);
};

class PlayerActionCondition : public Condition {
    public:
        PlayerActionCondition();
        PlayerActionCondition(const YAML::Node &node);

        virtual bool isMet(const WorldState &world);
};

class PlayerPositionCondition : public Condition {
    public:
        PlayerPositionCondition(const QPointF &ll, const QPointF &ur);
        PlayerPositionCondition(const YAML::Node &node);

        virtual bool isMet(const WorldState &world);

    protected:
        QPointF ll, ur;
};

class VarValueCondition : public Condition {
    public:
        VarValueCondition(const YAML::Node &node);

        virtual bool isMet(const WorldState &world);

    protected:
        enum class Operation {
            Less,
            LessEq,
            Eq,
            GreaterEq,
            Greater
        };

        QString var;
        int val;
        Operation op;
};

#endif
