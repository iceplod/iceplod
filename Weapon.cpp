/*
 *  Copyright 2011 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Weapon.h"

#include "Player.h"

#include <QList>
#include <QDebug>

class BasicBullet : public Actor {
    public:
        BasicBullet(QPointF pos, bool left);

        virtual void tick(const WorldState &ws);
        virtual void getDrawInfo(GameState &gs) const;

        bool shouldDie() const;

    protected:
        QPointF pos;
        QPointF vel;
        unsigned int timer;
};

BasicBullet::BasicBullet(QPointF pos, bool left):
    pos(pos),
    timer(100)
{
    if(left)
        vel.rx() = -0.5f;
    else
        vel.rx() = 0.5f;
}

void BasicBullet::tick(const WorldState &ws)
{
    if(timer > 0)
        --timer;

    pos += vel;
}

bool BasicBullet::shouldDie() const
{
    return (timer == 0);
}

void BasicBullet::getDrawInfo(GameState &gs) const
{
    AnimationState as;
    as.angle = 0;
    as.pos = pos;
    if(vel.x() >= 0)
        as.size.set(0.5f, 0.5f);
    else
        as.size.set(-0.5f, 0.5f);
    as.anim = StaticAnimations::BasicBulletAnim;
    gs.layer0Anims.push_back(as);
}

class BasicGun : public Weapon {
    public:
        BasicGun(const Player &player):
            Weapon(player),
            shoot_timer(0)
        {}

        virtual void tick(const WorldState &ws);

        virtual void getDrawInfo(GameState &gs) const;

    protected:
        QList<BasicBullet> bullets;

        unsigned int shoot_timer;
};

void BasicGun::tick(const WorldState &ws)
{
    for(QList<BasicBullet>::iterator bullet = bullets.begin();
            bullet != bullets.end(); ++bullet){
        bullet->tick(ws);

        if(bullet->shouldDie()){
            bullet = bullets.erase(bullet);
            if(bullet == bullets.end())
                break;
        }
    }

    if(shoot_timer > 0)
        --shoot_timer;
    else{
        if(player.isShooting()){
            bullets.push_back(BasicBullet(player.getHandPosition(),
                        player.getVelocity().x() < 0));
            shoot_timer = 10;
        }
    }
}

void BasicGun::getDrawInfo(GameState &gs) const
{
    for(QList<BasicBullet>::const_iterator bullet = bullets.constBegin();
            bullet != bullets.constEnd(); ++bullet)
        bullet->getDrawInfo(gs);

    AnimationState as;
    as.angle = 0;
    as.pos = player.getHandPosition();
    if(player.isFacingRight())
        as.size.set(1.f, 1.f);
    else
        as.size.set(-1.f, 1.f);
    as.anim = StaticAnimations::BasicGunAnim;
    gs.layer0Anims.push_back(as);
}

WeaponPtr Weapon::Create(Weapon::Type type, const Player &player)
{
    switch(type){
        case Type::BasicGun:
            {
                WeaponPtr p(new BasicGun(player));
                return p;
            }
        default:
            qDebug() << "Unknown weapon type:" << (int)type;
            return WeaponPtr();
    }
}
