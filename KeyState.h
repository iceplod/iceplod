/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KEYSTATE_H
#define KEYSTATE_H

#include <QMutex>

class KeyState {
    public:
        KeyState():
            down(false), up(false), left(false), right(false), escape(false),
            action(false), holdingAction(false), shoot(false)
        {}

        bool down;
        bool up;
        bool left;
        bool right;

        bool escape;

        bool action, holdingAction;

        bool shoot;

        static KeyState GetKeyState()
        {
            QMutexLocker lk(&lock);
            return curState;
        }

        static void SetDown(bool to)
        {
            QMutexLocker lk(&lock);
            curState.down = to;
        }

        static void SetUp(bool to)
        {
            QMutexLocker lk(&lock);
            curState.up = to;
        }

        static void SetLeft(bool to)
        {
            QMutexLocker lk(&lock);
            curState.left = to;
        }

        static void SetRight(bool to)
        {
            QMutexLocker lk(&lock);
            curState.right = to;
        }

        static void SetEscape(bool to)
        {
            QMutexLocker lk(&lock);
            curState.escape = to;
        }

        static void SetAction(bool to)
        {
            QMutexLocker lk(&lock);
            curState.action = to;
        }

        static void SetShoot(bool to)
        {
            QMutexLocker lk(&lock);
            curState.shoot = to;
        }

    protected:
        static KeyState curState;
        static QMutex lock;
};

#endif
