/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRUSHDOCKCONTENTS_H
#define BRUSHDOCKCONTENTS_H

#include <QWidget>

#include "LevelSet.h"
#include "Map.h"
#include "Tile.h"

class Tool : public QObject {
    Q_OBJECT

    public:
        virtual void work(Map &map, const QPointF &pos,
               Qt::MouseButtons buttons, bool dragged) = 0;

        virtual void getDrawInfo(GameState &gs, const QPointF &mouse);
};

class ToolContents;

class ToolDockContents : public QWidget {
    Q_OBJECT

    public:
        ToolDockContents(QWidget *parent = NULL);

        void emitTool();

        void reset();
        void hookupLevelSet(LevelSetPtr ls);

    signals:
        void newTool(Tool *tool);
        void toolNameChanged(QString name);

    protected slots:
        void newToolSelected(ToolContents *newContents);
        void brushToolSelected();
        void portalToolSelected();
        void copyToolSelected();
        void enemyToolSelected();

    protected:
        ToolContents *toolContents;

        QPushButton *brushBut, *portalBut, *copyBut, *enemyBut;
        QPushButton *selectedBut;

        LevelSetPtr ls;
};

#endif
