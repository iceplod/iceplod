/*
 *  Copyright 2011 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENEMY_H
#define ENEMY_H

#include "Actor.h"
#include <boost/shared_ptr.hpp>

class Enemy;

typedef boost::shared_ptr<Enemy> EnemyPtr;

class Enemy : public Actor {
    public:
        enum class Type : quint32 {
            Walker,
            FirstEnemy = Walker,
            LastEnemy
        };

        static EnemyPtr Create(Type type, const QPointF &pos);
        static QString GetEnemyName(Type type);
        static void GetStaticDrawInfo(Type type, QPointF pos, GameState &gs);

    protected:
        Enemy(const QPointF &pos);

        /* convenience methods for physics-respecting enemies */
        bool checkBottom(const WorldState &ws);
        bool checkTop(const WorldState &ws);
        bool checkLeft(const WorldState &ws);
        bool checkRight(const WorldState &ws);

        /* bottom-center of the enemy */
        QPointF pos;

        QPointF vel;
        Rect<float> size;
};

#endif
