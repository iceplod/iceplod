/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDITOR_H
#define EDITOR_H

#include "LevelSet.h"
#include "Map.h"
#include "Renderer.h"

#include <QMainWindow>

class ToolDockContents;
class Tool;
class MapDockContents;

class Editor : public QMainWindow {
    Q_OBJECT

    public:
        Editor(QString filename, QWidget *parent = NULL);

    public slots:
        void newLevelSet();
        void load();
        void save();
        void testLevel();

        void resize();
        void editPortals();
        void editLevelSetProperties();
        void editResources();

        void mapFilenameChanged(QString newFilename);
        void mapSelected(MapPtr map);

        void displayError(QString error);

        void zoomChanged(int zoom);

        void resendGameState();
        void newTool(Tool *tool);

        void rendererMouseMoveEvent(QMouseEvent *event);

    protected:
        Renderer renderer;
        LevelSetPtr levelset;
        MapPtr map;
        QPointF cameraFocus, lastMousePos, lastMouseMapCoord;
        Tool *tool;

        QDockWidget *brushDock, *mapDock;
        ToolDockContents *toolDockContents;
        MapDockContents *mapDockContents;
        QAction *toggleGridAct, *togglePhysAct;
        QSlider *zoomSlider;
        QLabel *zoomLabel, *coordsLabel;

        void mousePressEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void processMoveEvent(QMouseEvent *event, bool shove);
        void wheelEvent(QWheelEvent *event);

        void loadLevelSet(QString filename, bool create = false);

        void closeEvent(QCloseEvent *event);

    private:
        void setupDocks();
        void setupMenus();
        void setupStatusBar();
};

#endif
