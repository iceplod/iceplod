/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERER_H
#define RENDERER_H

#include <QtOpenGL>

#include "Animation.h"
#include "GameState.h"
#include "Math.h"
#include "Tile.h"

class Renderer : public QGLWidget {
    Q_OBJECT

    public:
        Renderer(QWidget *parent = NULL);

        QPointF mapToGame(const QPoint &pos) const;
        QPointF mapFromParentToGame(const QPoint &pos) const;

        Rect<float> tilesOnScreen() const;

        const static Rect<int> GameViewport;
        const static int PixelsPerUnit;
        const static Rect<float> UnitsPerDialogCharacter;
        const static int DialogCharactersPerLine;
        const static int LinesPerDialog;

    signals:
        void error(QString str);
        void mouseMove(QMouseEvent *event);

    public slots:
        void addUserAnim(AnimationHandle id, QString name, QString filename);
        void deleteUserAnim(AnimationHandle id);
        void resetUserAnims();

    protected:
        AnimationPtrList staticAnims;
        QHash<AnimationHandle, AnimationPtr> dynamicAnims;
        FramePtr crudeFont;

        void initializeGL();
        void loadStaticAnims();
        void resizeGL(int w, int h);
        void paintGL();

        void landscapeMode(GameState &gameState);

        void renderAnims(const AnimationStateList &anims);
        void renderLines(const LineList &lines);
        void renderTexts(const QList<TextItem> &texts);
        void renderText(const TextItem &text);
        void renderDialog(const DialogInfo &dialog);

        void mouseMoveEvent(QMouseEvent *event);

        Rect<int> determineTextDimensions(const char *text);
};

#endif
