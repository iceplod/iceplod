/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Tile.h"

const float Tile::ShoveThreshold = 0.001f;

class IgnoreTile : public Tile {
    public:
        IgnoreTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::Ignore, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::Nil;
        }
};

class SolidTile : public Tile {
    public:
        SolidTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::Solid, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::Solid;
        }

        bool shoveToTop(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() + 1.f - pt.y();
            return shove.y() > ShoveThreshold;
        }

        bool shoveToBottom(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() - pt.y();
            return shove.y() < -ShoveThreshold;
        }

        bool shoveToRight(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() + 1.f - pt.x();
            shove.ry() = 0;
            return shove.x() > ShoveThreshold;
        }

        bool shoveToLeft(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() - pt.x();
            shove.ry() = 0;
            return shove.x() < -ShoveThreshold;
        }
};

class UpLowTile : public Tile {
    public:
        UpLowTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::UpLow, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::UpLow;
        }

        bool shoveToTop(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() + 0.5f * (pt.x() - pos.x()) - pt.y();
            return shove.y() > ShoveThreshold;
        }

        bool shoveToBottom(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() - pt.y();
            return shove.y() < -ShoveThreshold;
        }

        bool shoveToRight(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() + 1.f - pt.x();
            shove.ry() = 0;
            return shove.x() > ShoveThreshold;
        }

        bool shoveToLeft(const QPointF &pt, QPointF &shove) const
        {
            return false;
        }
};

class UpHighTile : public Tile {
    public:
        UpHighTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::UpHigh, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::UpHigh;
        }

        bool shoveToTop(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() + 0.5f + 0.5f * (pt.x() - pos.x()) - pt.y();
            return shove.y() > ShoveThreshold;
        }

        bool shoveToBottom(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() - pt.y();
            return shove.y() < -ShoveThreshold;
        }

        bool shoveToRight(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() + 1.f - pt.x();
            shove.ry() = 0;
            return shove.x() > ShoveThreshold;
        }

        bool shoveToLeft(const QPointF &pt, QPointF &shove) const
        {
            return false;
        }
};

class DownLowTile : public Tile {
    public:
        DownLowTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::DownLow, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::DownLow;
        }

        bool shoveToTop(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() + 0.5f - 0.5f * (pt.x() - pos.x()) - pt.y();
            return shove.y() > ShoveThreshold;
        }

        bool shoveToBottom(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() - pt.y();
            return shove.y() < -ShoveThreshold;
        }

        bool shoveToRight(const QPointF &pt, QPointF &shove) const
        {
            return false;
        }

        bool shoveToLeft(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() - pt.x();
            shove.ry() = 0;
            return shove.x() < -ShoveThreshold;
        }
};

class DownHighTile : public Tile {
    public:
        DownHighTile(const QPointF &pos, AnimationHandle anim):
            Tile(pos, Type::DownHigh, anim)
        {}

        AnimationHandle getPhysAnim() const
        {
            return StaticAnimations::DownHigh;
        }

        bool shoveToTop(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() + 1.f - 0.5f * (pt.x() - pos.x()) - pt.y();
            return shove.y() > ShoveThreshold;
        }

        bool shoveToBottom(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = 0;
            shove.ry() = pos.y() - pt.y();
            return shove.y() < -ShoveThreshold;
        }

        bool shoveToRight(const QPointF &pt, QPointF &shove) const
        {
            return false;
        }

        bool shoveToLeft(const QPointF &pt, QPointF &shove) const
        {
            shove.rx() = pos.x() - pt.x();
            shove.ry() = 0;
            return shove.x() < -ShoveThreshold;
        }
};

Tile::Tile(const QPointF &pos, Type type, AnimationHandle anim):
    pos(pos),
    type(type),
    anim(anim)
{}

ConstTilePtr Tile::Create(const QPointF &pos, Type type, AnimationHandle anim)
{
    switch(type){
        case Type::Ignore:
            {
                ConstTilePtr p(new IgnoreTile(pos, anim));
                return p;
            }
        case Type::Solid:
            {
                ConstTilePtr p(new SolidTile(pos, anim));
                return p;
            }
        case Type::UpLow:
            {
                ConstTilePtr p(new UpLowTile(pos, anim));
                return p;
            }
        case Type::UpHigh:
            {
                ConstTilePtr p(new UpHighTile(pos, anim));
                return p;
            }
        case Type::DownLow:
            {
                ConstTilePtr p(new DownLowTile(pos, anim));
                return p;
            }
        case Type::DownHigh:
            {
                ConstTilePtr p(new DownHighTile(pos, anim));
                return p;
            }
        default:
            qDebug() << "Unknown tile type:" << (int)type;
            return ConstTilePtr();
    }
}

void Tile::getDrawInfo(GameState &gs) const
{
    if(anim == StaticAnimations::Unset)
        return;

    AnimationState as;
    as.pos.rx() = pos.x() + 0.5f;
    as.pos.ry() = pos.y() + 0.5f;
    as.size.set(1, 1);
    as.anim = anim;
    gs.layer0Anims.push_back(as);
}

void Tile::getPhysDrawInfo(GameState &gs) const
{
    AnimationState as;

    as.anim = getPhysAnim();
    if(as.anim == StaticAnimations::Nil)
        return;

    as.pos.rx() = pos.x() + 0.5f;
    as.pos.ry() = pos.y() + 0.5f;
    as.size.set(1, 1);
    as.alpha = 0.7f;

    gs.layer3Anims.push_back(as);
}

void Tile::write(QDataStream &out) const
{
    out << (quint32)type << (quint32)anim;
}

ConstTilePtr Tile::Read(const QPointF &pos, QDataStream &in)
{
    quint32 type, anim;
    in >> type >> anim;
    return Create(pos, (Type)type, anim);
}
