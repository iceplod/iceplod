/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGTREE_H
#define DIALOGTREE_H

#include "GameState.h"

#include <QList>
#include <QString>
#include <boost/shared_ptr.hpp>
#include <yaml-cpp/yaml.h>

class DialogItem;
typedef boost::shared_ptr<DialogItem> DialogItemPtr;

class DialogChoice {
    public:
        DialogChoice(const YAML::Node &node,
                QHash<QString, DialogItemPtr> &itemsById);

        QString choice;
        QList<DialogItemPtr> items;
};

class DialogItem {
    public:
        DialogItem(const YAML::Node &node,
                QHash<QString, DialogItemPtr> &itemsById);

        void setupChildren(QList<DialogItemPtr>::const_iterator thisItem,
                QList<DialogItemPtr>::const_iterator thisEnd);

        QString id;
        QString text;
        QString forceTrigger;
        QString goTo;
        QList<DialogChoice> options;

        bool root;
        QList<DialogItemPtr>::const_iterator parentItem, parentEnd;
        QList<DialogItemPtr>::const_iterator thisItem, thisEnd;
};

class DialogTree {
    public:
        DialogTree(const YAML::Node &node);

        const QString &getName() { return name; }
        bool updateState(QList<QString> &forcedTriggers);
        void getDrawInfo(GameState &gs) const;

    protected:
        QString name;
        QList<DialogItemPtr> items;
        QHash<QString, DialogItemPtr> itemsById;

        void flowText();

    private:
        bool active;
        int selection;
        int textOffs;
        QString flowedText;

        QList<DialogItemPtr>::const_iterator curItem;
        QList<DialogItemPtr>::const_iterator curEnd;
};

typedef boost::shared_ptr<DialogTree> DialogTreePtr;

#endif
