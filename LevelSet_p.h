/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVELSET_P_H
#define LEVELSET_P_H

#include "ExitData.h"

#include <QAbstractTableModel>

class LSPortalModel : public QAbstractTableModel {
    Q_OBJECT

    public:
        LSPortalModel(const QHash<quint32, ExitData> &portals, bool isStartMap,
                quint32 startPortal, QObject *parent = NULL);

        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        int columnCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const;
        bool setData(const QModelIndex &index, const QVariant &value,
                int role = Qt::EditRole);
        Qt::ItemFlags flags() const;

        QHash<quint32, ExitData> portals;
        bool isStartMap;
        quint32 startPortal;
};

#endif
