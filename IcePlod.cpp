/*
 *  Copyright 2010 Andrew Eikum
 *
 *  This file is part of IcePlod.
 *
 *  IcePlod is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  IcePlod is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with IcePlod.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IcePlod.h"

IcePlod::IcePlod(QString filename, QWidget *parent):
    QMainWindow(parent),
    sim(filename),
    holdingAction(false)
{
    setWindowTitle("IcePlod");

    connect(&renderer, SIGNAL(error(QString)),
            this, SLOT(displayError(QString)));
    setCentralWidget(&renderer);

    statusBar()->setSizeGripEnabled(false);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), &renderer, SLOT(updateGL()));
    timer->start(16.6);

    connect(&sim, SIGNAL(error(QString)), this, SLOT(displayError(QString)));

    qRegisterMetaType<AnimationHandle>("AnimationHandle");
    connect(&sim, SIGNAL(animationAdded(AnimationHandle, QString, QString)),
            &renderer, SLOT(addUserAnim(AnimationHandle, QString, QString)));

    sim.start();
}

IcePlod::~IcePlod()
{
    sim.quit();
    sim.wait();
}

bool IcePlod::handleKey(int key, bool down)
{
    switch(key){
    case Qt::Key_Up:
        KeyState::SetUp(down);
        return true;
    case Qt::Key_Down:
        KeyState::SetDown(down);
        return true;
    case Qt::Key_Left:
        KeyState::SetLeft(down);
        return true;
    case Qt::Key_Right:
        KeyState::SetRight(down);
        return true;
    case Qt::Key_Escape:
        KeyState::SetEscape(down);
        return true;
    case Qt::Key_A:
        if(down){
            if(!holdingAction){
                KeyState::SetAction(true);
                holdingAction = true;
            }
        }else
            holdingAction = false;
        return true;
    case Qt::Key_S:
        KeyState::SetShoot(down);
        return true;
    default:
        break;
    }

    return false;
}

void IcePlod::keyPressEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat() || !handleKey(event->key(), true))
        QMainWindow::keyPressEvent(event);
}

void IcePlod::keyReleaseEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat() || !handleKey(event->key(), false))
        QMainWindow::keyReleaseEvent(event);
}

void IcePlod::displayError(QString str)
{
    qDebug() << str;
    QMessageBox msg;
    msg.setWindowTitle("Error");
    msg.setIcon(QMessageBox::Warning);
    msg.setText(str);
    msg.exec();
}

int main(int argc, char ** argv)
{
    QApplication app(argc, argv);

    IcePlod win(argv[1]);
    win.show();

    return app.exec();
}
